package org.vacoor.nothing.ui.hotkey;

import java.awt.event.KeyEvent;

/**
 * User: Vacoor
 */
public class OSHotKeyEvent {
    private int identifer;
    private int modifier;
    private int key;

    public OSHotKeyEvent(int identifer, int modifier, int key) {
        this.identifer = identifer;
        this.modifier = modifier;
        this.key = key;
    }

    public int getIdentifer() {
        return identifer;
    }

    public int getModifier() {
        return modifier;
    }

    public int getKey() {
        return key;
    }

    public String getHotKeyText() {
        return KeyEvent.getKeyModifiersText(modifier) + "+" + KeyEvent.getKeyText(key);
    }
}
