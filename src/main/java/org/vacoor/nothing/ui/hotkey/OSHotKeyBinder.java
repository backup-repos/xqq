package org.vacoor.nothing.ui.hotkey;

/**
 * 系统热键绑定器
 *
 * @author: Vacoor
 */
public interface OSHotKeyBinder {

    /**
     * 获取当前绑定器标示符, 标示符一般表示一个功能
     *
     * @return
     */
    int getIdentifier();

    /**
     * 获取热键绑定器的修饰键
     * {@link java.awt.event.KeyEvent}
     *
     * @return
     */
    int getModifiers();

    /**
     * 获取按键绑定器的按键
     * {@link java.awt.event.KeyEvent}
     *
     * @return
     */
    int getKey();

    /**
     * 设置热键
     * eg: setHotKey(KeyEvent.CONTROL_MASK + KeyEvent.ALT_MASK, KeyEvent.VK_Z)
     * {@link java.awt.event.KeyEvent}
     *
     * @param midifier
     * @param key
     * @return 是否绑定成功
     */
    boolean setHotKey(int midifier, int key);

    /**
     * 添加一个热键监听
     *
     * @param listener
     */
    void addOSHotKeyListener(org.vacoor.nothing.ui.hotkey.OSHotKeyListener listener);

    void removeOSHotKeyListener(org.vacoor.nothing.ui.hotkey.OSHotKeyListener listener);

    OSHotKeyListener[] getOSHotKeyListeners();
}
