package org.vacoor.nothing.ui.hotkey.impl.linux;

import jxgrabkey.JXGrabKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vacoor.nothing.common.misc.util.Platform;
import org.vacoor.nothing.common.misc.util.Resources;
import org.vacoor.nothing.ui.hotkey.HotKeyDemo;
import org.vacoor.nothing.ui.hotkey.OSHotKeyBinder;
import org.vacoor.nothing.ui.hotkey.OSHotKeyManager;

import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Linux 平台系统级热键, 对JXGrabKey简单封装
 * JXGrabKey:
 * http://sourceforge.net/projects/jxgrabkey/files/jxgrabkey/
 * <p/>
 * <p/>
 * User: Vacoor
 */
public class JXGrabKeyManager extends OSHotKeyManager {
    private static final Logger logger = LoggerFactory.getLogger(org.vacoor.nothing.ui.hotkey.impl.linux.JXGrabKeyManager.class);
    private static final String LINUX_64_LIB = "native_lib/JXGrabKey_amd64.so";
    private static final String LINUX_32_LIB = "native_lib/JXGrabKey_i386.so";
    private static final String EXTRACT_FILE = System.getProperty("java.io.tmpdir") + "/" + Platform.getResourcePrefix() + "-hotkey.so";

    private static ConcurrentHashMap<Integer, OSHotKeyBinder> binders = new ConcurrentHashMap<Integer, OSHotKeyBinder>();
    private static JXGrabKey jxGrabKey;
    private static boolean initialzed;

    private static void init() {
        if (!initialzed && Platform.isLinux()) {
            try {
                String lib = Platform.is64Bit() ? LINUX_64_LIB : LINUX_32_LIB;
                lib = Resources.extractResourceAsCanonicalPath(lib, EXTRACT_FILE);
                System.load(lib);
                jxGrabKey = JXGrabKey.getInstance();
            } catch (Throwable t) {
                logger.warn("JXGrab load error", t);
            }
        }
        initialzed = true;
    }

    static URL getResource(String path) {
        return HotKeyDemo.class.getClassLoader().getResource(path);
    }

    @Override
    public boolean supportCurrentOS() {
        if (!initialzed) {
            init();
        }
        return jxGrabKey != null;
    }

    @Override
    public OSHotKeyBinder getOSHotKeyBinder(int identifier) {
        if (!supportCurrentOS()) {
            return null;
        }
        OSHotKeyBinder binder = binders.get(identifier);
        if (binder != null) {
            return binder;
        }
        synchronized (binders) {
            if (binder == null) {
                binder = new JXGrabKeyBinder(jxGrabKey, identifier);
                binders.put(identifier, binder);
            }
        }
        return binder;
    }

    @Override
    public void destory() {
        if (jxGrabKey != null) {
            /*
            for(int identifier : binders.keySet()) {
                jxGrabKey.unregisterHotKey(identifier);
            }
            */
            jxGrabKey.cleanUp();
        }
    }
}
