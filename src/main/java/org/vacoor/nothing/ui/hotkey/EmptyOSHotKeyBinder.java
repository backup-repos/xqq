package org.vacoor.nothing.ui.hotkey;

import javax.swing.event.EventListenerList;

/**
 * 空白绑定器
 * <p/>
 * User: Vacoor
 */
public abstract class EmptyOSHotKeyBinder implements OSHotKeyBinder {
    protected EventListenerList listeners = new EventListenerList();
    protected final int identifier;
    protected int modifiers;
    protected int key;

    public EmptyOSHotKeyBinder(int identifier) {
        this.identifier = identifier;
    }

    @Override
    public int getIdentifier() {
        return identifier;
    }

    @Override
    public int getModifiers() {
        return modifiers;
    }

    @Override
    public int getKey() {
        return key;
    }

    @Override
    public boolean setHotKey(int modifiers, int key) {
        this.modifiers = modifiers;
        this.key = key;
        return doRegisterHotKey(modifiers, key);
    }

    protected abstract boolean doRegisterHotKey(int modifiers, int key);

    @Override
    public void addOSHotKeyListener(OSHotKeyListener listener) {
        listeners.add(OSHotKeyListener.class, listener);
    }

    @Override
    public void removeOSHotKeyListener(OSHotKeyListener listener) {
        listeners.remove(OSHotKeyListener.class, listener);
    }

    @Override
    public OSHotKeyListener[] getOSHotKeyListeners() {
        return listeners.getListeners(OSHotKeyListener.class);
    }
}
