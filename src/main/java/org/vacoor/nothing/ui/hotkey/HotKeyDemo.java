package org.vacoor.nothing.ui.hotkey;

import java.awt.event.KeyEvent;

public class HotKeyDemo {

    public static void main(String[] args) {
        org.vacoor.nothing.ui.hotkey.OSHotKeyManager manager = org.vacoor.nothing.ui.hotkey.OSHotKeyManager.getCurrentOSHotKeyManager();
        if (manager == null) {
            System.err.println("没有合适的热键适配器");
            return;
        }

        OSHotKeyBinder binder = manager.getOSHotKeyBinder(OSHotKeyManager.nextIdentifier());
        System.out.println(binder.setHotKey(KeyEvent.CTRL_MASK + KeyEvent.ALT_MASK, KeyEvent.VK_Z));

        binder.addOSHotKeyListener(new OSHotKeyListener() {
            @Override
            public void onOSHotKey(OSHotKeyEvent event) {
                System.out.println("全局快捷键被按下:" + event.getHotKeyText());
            }
        });
        System.out.println("你可以在任意焦点按下 CTRL + ALT + C");
//        manager.destory();
    }
}
