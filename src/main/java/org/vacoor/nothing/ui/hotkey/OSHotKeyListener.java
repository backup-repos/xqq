package org.vacoor.nothing.ui.hotkey;

import java.util.EventListener;

/**
 * User: Vacoor
 */
public interface OSHotKeyListener extends EventListener {
    void onOSHotKey(OSHotKeyEvent event);
}
