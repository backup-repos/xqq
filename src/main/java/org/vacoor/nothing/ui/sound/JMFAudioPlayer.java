package org.vacoor.nothing.ui.sound;

import javax.media.*;
import java.net.URL;

/**
 * User: vacoor
 */
public abstract class JMFAudioPlayer {

    private JMFAudioPlayer() {
    }

    public static void play(URL url) {
        Player player;
        try {
            player = Manager.createPlayer(url);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        final Player p = player;
        player.addControllerListener(new ControllerAdapter() {
            @Override
            public void stop(StopEvent e) {
                super.stop(e);
                p.close();
            }

            @Override
            public void controllerError(ControllerErrorEvent e) {
                p.close();
            }
        });

        // 这里本来就是多线程, 不需再创建线程
        player.start();
    }

    /*
        String driver = "vfw:Logitech USB Video Camera:0";
        String driver = "vfw:Microsoft WDM Image Capture (Win32):0";
        CaptureDeviceInfo device = CaptureDeviceManager.getDevice(driver);
        Vector<CaptureDeviceInfo> deviceList = CaptureDeviceManager.getDeviceList(null);
        device.getLocator();
        V4L4J
     */
}
