package org.vacoor.nothing.ui.util;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * UI 工具类, 设置主题和字体
 *
 * @author: Vacoor
 */
public abstract class UIs {
    // 最佳字体列表
    private static final String[] BEST_FONT_FAMILIES = {"Microsoft YaHei", "微软雅黑", "WenQuanYi Micro Hei", "文泉驿微米黑"};
    private static final String FALLBACK_FONT_FAMILY = Font.SANS_SERIF;
    private static final int BEST_FONT_SIZE = 12; // 说的是磅值, 但是怎么感觉是像素, 总是有偏差, 12 差不多是10号字体

    // --------------
    private static final String[] AVALIABLE_FONT_FAMILY_NAMES;
    private static final Font BEST_FONT;

    static {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        AVALIABLE_FONT_FAMILY_NAMES = env.getAvailableFontFamilyNames();

        final List<String> fontFamilyNames = Arrays.asList(AVALIABLE_FONT_FAMILY_NAMES);
        Font f = null;
        for (String bestFontName : BEST_FONT_FAMILIES) {
            if (fontFamilyNames.contains(bestFontName)) {
                f = new Font(bestFontName, Font.PLAIN, BEST_FONT_SIZE);
                break;
            }
        }
        BEST_FONT = f != null ? f : new Font(FALLBACK_FONT_FAMILY, Font.PLAIN, BEST_FONT_SIZE);
    }

    /**
     * 获取最佳字体, 如果需要修改 style, size 可以通过 {@link java.awt.Font#deriveFont(int, float)}
     *
     * @return
     */
    public static Font getBestFont() {
        return BEST_FONT;
    }

    /**
     * 获取当前可用 FontFamily 名称
     *
     * @return
     */
    public static String[] getAvaliableFontFamilyNames() {
        return AVALIABLE_FONT_FAMILY_NAMES;
    }

    /**
     * 安装最佳 LookAndFeel, 将会调用 {@link #installGlobalFont(java.awt.Font)}
     */
    public static void installBestLookAndFeel() {
        try {
            // 只在 Nimbus 下启用
            enableAntiAliasingUseSysSetting(!BEST_FONT.getFamily().equalsIgnoreCase(FALLBACK_FONT_FAMILY));
            enableSwingAntiAliasing(true);
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception ignore) {
            enableAntiAliasingUseSysSetting(false);
            enableSwingAntiAliasing(true);
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ignore2) {
            }
        }

        installGlobalFont(BEST_FONT);
    }

    /**
     * 应该在 设置 LookAndFeel 后设置
     *
     * @param font
     */
    public static void installGlobalFont(Font font) {
        for (Map.Entry<Object, Object> entry : UIManager.getDefaults().entrySet()) {
            Object value = entry.getValue();
            if (!(value instanceof FontUIResource)) {
                continue;
            }
            FontUIResource oldFontRes = (FontUIResource) value;
            entry.setValue(new FontUIResource(font.getName(), oldFontRes.getStyle(), Math.max(oldFontRes.getSize(), font.getSize())));
        }
    }

    /**
     * 启用Swing抗锯齿,
     */
    private static void enableSwingAntiAliasing(boolean enable) {
        System.setProperty("swing.aatext", enable ? "true" : "false");
    }

    /**
     * 使用系统抗锯齿设置, 暂时只发现对 Nimbus 主题 有正向效果
     */
    private static void enableAntiAliasingUseSysSetting(boolean enable) {
        System.setProperty("awt.useSystemAAFontSettings", enable ? "on" : "off");
    }

    private UIs() {
    }
}