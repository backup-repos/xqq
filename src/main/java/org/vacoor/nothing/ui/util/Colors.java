package org.vacoor.nothing.ui.util;

import org.vacoor.nothing.common.codec.Hex;
import org.vacoor.nothing.common.util.Bytes;

import java.awt.*;

/**
 * 颜色转换工具类
 * <p/>
 *
 * @author vacoor
 */
public abstract class Colors {

    public static Color parseHtmlHexColor(String htmlColor) {
        if (htmlColor == null || !htmlColor.matches("#?(?:[0-9A-Fa-f]{3}|[0-9A-Fa-f]{6})")) {
            return Color.BLACK;
        }
        return new Color(Bytes.toInt(Hex.decode(htmlColor.replace("#", ""))));
    }

    public static String toHtmlHex(Color color) {
        return "#" + Hex.encode(Bytes.toBytes(color.getRGB() | 0xFF000000)); // clear alpha
    }

    public static String toHex(Color color) {
        String hex = Hex.encode(Bytes.toBytes(color.getRGB()));
        return hex.startsWith("FF") ? hex.substring(2) : hex;
    }

    private Colors() {
    }
}