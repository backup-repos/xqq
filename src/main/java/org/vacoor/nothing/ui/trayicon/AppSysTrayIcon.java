package org.vacoor.nothing.ui.trayicon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * @author: vacoor
 */
public abstract class AppSysTrayIcon {
    private static final Logger logger = LoggerFactory.getLogger(org.vacoor.nothing.ui.trayicon.AppSysTrayIcon.class);
    private static TrayAnimatedIcon trayIcon;

    private AppSysTrayIcon() {
    }

    public static TrayAnimatedIcon getCurrentAppTrayAnimatedIcon() {
        return trayIcon;
    }

    /**
     * 创建一个托盘图标并返回, 如果不支持或失败返回null
     * 当已经存在一个托盘图标, 则设置属性会返回该对戏那个
     * <p/>
     *
     * @param image
     * @param tooltip
     * @param popup
     * @return
     */
    public static TrayAnimatedIcon addAppTrayAnimatedIcon(Image image, String tooltip, PopupMenu popup) {
        if (trayIcon != null) {
            trayIcon.setImage(image);
            trayIcon.setToolTip(tooltip);
            trayIcon.setPopupMenu(popup);
            return trayIcon;
        }
        if (!SystemTray.isSupported()) {
            return null;
        }

        synchronized (org.vacoor.nothing.ui.trayicon.AppSysTrayIcon.class) {
            if (trayIcon == null) {
                trayIcon = new TrayAnimatedIcon(image, tooltip, popup);
                trayIcon.setImageAutoSize(true);

                try {
                    SystemTray.getSystemTray().add(trayIcon);
                    trayIcon.setImage(image);
                    trayIcon.setToolTip(tooltip);
                    trayIcon.setPopupMenu(popup);
                } catch (AWTException e) {
                    logger.warn("add system tray fail: {}", e);
                    trayIcon = null;
                }
            }
        }
        return trayIcon;
    }
}
