package org.vacoor;

//import com.sun.java.swing.Painter;
import javax.swing.Painter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vacoor.nothing.ui.util.UIs;
import org.vacoor.nothing.ui.util.WindowMoveHandler;
import org.vacoor.xqq.Login3;
import org.vacoor.xqq.ui.comp.tabpane.FlatTabbedPaneUI;
import org.vacoor.xqq.ui.login.LoginPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author Vacoor
 */
public class Launcher {
    private static class FillPainter implements Painter<JComponent> {

        private final Color color;

        public FillPainter(Color c) {
            color = c;
        }

        /*
        public void paint(Graphics2D g, Object object, int width, int height) {
            g.setColor(color);
            g.fillRect(0, 0, width - 1, height - 1);
        }
        */

        // @Override
        public void paint(Graphics2D g, JComponent object, int width, int height) {
            g.setColor(color);
            g.fillRect(0, 0, width - 1, height - 1);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(Launcher.class);

    private static final ImageIcon LOADING_ICON = new ImageIcon(Launcher.class.getClassLoader().getResource("images/loading.gif"));
    private static final String LOGIN_PANEL_KEY = Launcher.class.getName() + ".panel.login";
    private static final String LOADING_LABEL_KEY = Launcher.class.getName() + ".label.loading";
    private static final String LOADING_TEXT = "登录...";

    public static String getLookAndFeel() {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    return info.getClassName();
                }
            }
        } catch (Exception ignore) {
        }
        return UIManager.getCrossPlatformLookAndFeelClassName();
    }

    public static void main(String[] args) throws InterruptedException {
         UIs.installBestLookAndFeel();
//        UIManager.put("TreeUI", CategoryTreeUI.class.getCanonicalName());
        UIManager.put("TabbedPaneUI", FlatTabbedPaneUI.class.getCanonicalName());
        UIManager.put("ScrollBar.width", 5);

        UIManager.getLookAndFeelDefaults().put("ScrollBar:ScrollBarThumb[Enabled].backgroundPainter", new FillPainter(new Color(127, 169, 191)));
        UIManager.getLookAndFeelDefaults().put("ScrollBar:ScrollBarThumb[MouseOver].backgroundPainter", new FillPainter(new Color(127, 169, 191)));
        UIManager.getLookAndFeelDefaults().put("ScrollBar:ScrollBarTrack[Enabled].backgroundPainter", new FillPainter(new Color(190, 212, 223)));

        UIManager.getLookAndFeelDefaults().put("ScrollBar:\"ScrollBar.button\".size", 0);
        UIManager.getLookAndFeelDefaults().put("ScrollBar.decrementButtonGap", 0);
        UIManager.getLookAndFeelDefaults().put("ScrollBar.incrementButtonGap", 0);

        final JDialog frame = new JDialog();

        final CardLayout card = new CardLayout(0, 0);
        final JPanel boxPanel = new JPanel(card);
        final LoginPanel loginPanel = new LoginPanel();
        final JLabel loadingLabel = new JLabel(LOADING_TEXT, LOADING_ICON, JLabel.LEFT);

        boxPanel.setBackground(Color.WHITE);
        boxPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        loginPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 40, 20));
        loadingLabel.setOpaque(false);

        boxPanel.add(LOGIN_PANEL_KEY, loginPanel);
        boxPanel.add(LOADING_LABEL_KEY, loadingLabel);

        frame.setContentPane(boxPanel);
        frame.setResizable(false);
        frame.setUndecorated(true);
        frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        new WindowMoveHandler().bindTo(loginPanel);
        new WindowMoveHandler().bindTo(loadingLabel);

        loginPanel.setLoginAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                card.show(boxPanel, LOADING_LABEL_KEY);
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            Login3.login(loginPanel.getUsername(), loginPanel.getPasswd(), loginPanel.getStatus());
                            frame.dispose();
                        } catch (Exception e1) {
                            logger.warn("登陆异常:", e1);
                            loginPanel.clearPassword();
                            loadingLabel.setText("<html><body><span style='word-wrap: break-word;word-break: normal;'>" + e1.getMessage() + "</span></body></html>");
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e2) {
                            }
                            loadingLabel.setText(LOADING_TEXT);
                            card.show(boxPanel, LOGIN_PANEL_KEY);
                        }

                    }
                }.start();
            }
        });
    }
}
