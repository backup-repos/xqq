package org.vacoor.xqq.reply;

/**
 */
public interface ResponseResolver<T> {

    T resolveReply(String reply) throws ResolveException;

}
