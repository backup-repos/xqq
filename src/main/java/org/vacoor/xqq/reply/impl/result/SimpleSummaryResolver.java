package org.vacoor.xqq.reply.impl.result;

import com.fasterxml.jackson.databind.JsonNode;
import org.vacoor.nothing.common.json.Jacksons;
import org.vacoor.xqq.core.event.Summary;
import org.vacoor.xqq.reply.SummaryResolver;
import org.vacoor.xqq.reply.impl.JsonResolver;

/**
 * 签名解析器
 * {"retcode":0,"result":[{"uin":4156523229,"lnick":"__硪 锝 幸 福 、与 爱 情 无 关 、（却） 与 迩 冇 关"}]}
 */

public class SimpleSummaryResolver extends JsonResolver<Summary> implements SummaryResolver {

    @Override
    protected Summary doResolveResult(JsonNode result) {
        Summary[] summaries = Jacksons.deserialize(result.toString(), Summary[].class);
        return summaries.length > 0 ? summaries[0] : null;
    }

}
