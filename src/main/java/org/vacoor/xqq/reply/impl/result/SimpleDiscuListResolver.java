package org.vacoor.xqq.reply.impl.result;

import com.fasterxml.jackson.databind.JsonNode;
import org.vacoor.xqq.core.bean.Discussion;
import org.vacoor.xqq.reply.DiscuListResolver;
import org.vacoor.xqq.reply.impl.JsonResolver;

import java.util.ArrayList;
import java.util.List;

/**
 * {"retcode": 0,"result":{"dnamelist":[{"name": "Vacoor、HelloWorld、测试","did": 4185590391}]}}
 */
public class SimpleDiscuListResolver extends JsonResolver<List<Discussion>> implements DiscuListResolver {

    @Override
    protected List<Discussion> doResolveResult(JsonNode result) {
        List<Discussion> discus = new ArrayList<Discussion>();
        JsonNode list = result.get("dnamelist");
        if (list == null || !list.isArray()) {
            return discus;
        }

        for (JsonNode d : list) {
            long did = d.get("did").asLong();
            String name = d.get("name").asText();
            discus.add(createDiscussion(did, name));
        }
        return discus;
    }

    protected Discussion createDiscussion(long did, String name) {
        return new Discussion(did, name);
    }
}
