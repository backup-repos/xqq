package org.vacoor.xqq.reply.impl.result;

import com.fasterxml.jackson.databind.JsonNode;
import org.vacoor.nothing.common.json.Jacksons;
import org.vacoor.xqq.core.event.BuddyStatus;
import org.vacoor.xqq.reply.OnlineStatusResolver;
import org.vacoor.xqq.reply.impl.JsonResolver;

/**
 * 好友在线状态解析器
 * {"retcode": 0,"result": [{"uin": 803624437,"status": "online","client_type": 21},{"uin": 3265180940,"status": "busy","client_type": 1}]}
 */
public class SimpleOnlineStatusResolver extends JsonResolver<BuddyStatus[]> implements OnlineStatusResolver {

    @Override
    protected BuddyStatus[] doResolveResult(JsonNode result) {
        return Jacksons.deserialize(result.toString(), BuddyStatus[].class);
    }

}
