package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.event.BuddyStatus;

/**
 * 在线信息解析器
 */
public interface OnlineStatusResolver extends ResponseResolver<BuddyStatus[]> {
}
