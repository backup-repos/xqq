package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.bean.Group;

/**
 */
public interface GroupInfoResolver extends ResponseResolver<Group> {
}
