package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.exception.WebQQException;

/**
 */
public class ResolveException extends WebQQException {
    public ResolveException() {
        super();
    }

    public ResolveException(String message) {
        super(message);
    }

    public ResolveException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResolveException(Throwable cause) {
        super(cause);
    }
}
