package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.bean.Discussion;

import java.util.List;

/**
 * Created by Administrator on 13-12-28.
 */
public interface DiscuListResolver extends ResponseResolver<List<Discussion>> {
}
