package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.bean.Buddy;
import org.vacoor.xqq.core.bean.Category;

import java.util.List;

/**
 * 好友解析器
 */
public interface BuddiesResolver extends ResponseResolver<List<Category<Buddy>>> {
}
