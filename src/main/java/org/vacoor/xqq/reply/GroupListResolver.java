package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.bean.Group;

import java.util.List;

/**
 */
public interface GroupListResolver extends ResponseResolver<List<Group>> {
}
