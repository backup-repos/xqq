package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.event.Summary;

/**
 * 签名解析器
 */
public interface SummaryResolver extends ResponseResolver<Summary> {
}
