package org.vacoor.xqq.reply;

import org.vacoor.xqq.core.poll.PollResult;

/**
 */
public interface PollResolver extends ResponseResolver<PollResult> {

    @Override
    PollResult resolveReply(String reply) throws ResolveException;

}
