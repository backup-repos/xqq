package org.vacoor.xqq.ui.util;

import org.vacoor.nothing.ui.sound.WavPlayer;

import java.net.URL;

/**
 */
public abstract class QQAudioPlayer {
    private static final URL U_SYSTEM = QQAudioPlayer.class.getClassLoader().getResource("sound/system.wav");
    private static final URL U_MSG = QQAudioPlayer.class.getClassLoader().getResource("sound/msg.wav");
    private static final URL U_GLOBAL = QQAudioPlayer.class.getClassLoader().getResource("sound/global.wav");
    private static final URL U_AUDIO = QQAudioPlayer.class.getClassLoader().getResource("sound/audio.wav");

    public static void playSystemAudio() {
        WavPlayer.playWav(U_SYSTEM);
    }

    public static void playMsgAudio() {
        WavPlayer.playWav(U_MSG);
    }

    public static void playOnlineAudio() {
        WavPlayer.playWav(U_GLOBAL);
    }

    public static void playAudio() {
        WavPlayer.playWav(U_AUDIO);
    }

    private QQAudioPlayer() {
    }
}
