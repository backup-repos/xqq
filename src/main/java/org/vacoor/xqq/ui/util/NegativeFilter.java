package org.vacoor.xqq.ui.util;

import java.awt.*;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;

/**
 * 负片效果图片过滤器
 * <p/>
 *
 * @author: Vacoor
 */
public class NegativeFilter extends RGBImageFilter {

    public static Image createNegativeImage(Image image) {
        NegativeFilter filter = new NegativeFilter();
        ImageProducer prod = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(prod);
    }

    @Override
    public int filterRGB(int x, int y, int rgb) {
        int argb = rgb;
        int a = ((argb & 0xFF000000) >> 24); // alpha channel
        int r = 255 - ((argb & 0xFF0000) >> 16); // red channel
        int g = 255 - ((argb & 0xFF00) >> 8); // green channel
        int b = 255 - (argb & 0xFF); // blue channel
        return (a << 24 | (r << 16) | (g << 8) | b);
    }
}
