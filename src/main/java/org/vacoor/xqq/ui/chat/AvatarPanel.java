package org.vacoor.xqq.ui.chat;

import org.vacoor.xqq.core.bean.Peer;
import org.vacoor.xqq.ui.comp.border.RolloverBorder;
import org.vacoor.xqq.ui.util.ImageUtil;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * 聊天窗口顶部头像标题面板
 *
 * @author: Vacoor
 */
class AvatarPanel extends JPanel {
    public static final Dimension AVATAR_SIZE = new Dimension(30, 30);
    public static final Color PRIMARY_COLOR = Color.BLACK;
    public static final Color SECONDARY_COLOR = new Color(0x7F7F7F);

    private DataChangeMonitor monitor = new DataChangeMonitor();

    private JLabel avatar;
    private JLabel nick;
    private JLabel mark;
    private JLabel summary;
    private Peer peer;

    AvatarPanel() {
        init();
    }

    public AvatarPanel(Peer peer) {
        this.peer = peer;
        init();
        initDataAndRenderer(peer);
    }

    private void init() {
        this.avatar = new JLabel();
        this.nick = new JLabel();
        this.mark = new JLabel();
        this.summary = new JLabel();

        this.setLayout(new GridBagLayout());
        this.setOpaque(false);
        this.setBorder(null);

        avatar.setOpaque(false);
        nick.setOpaque(false);
        mark.setOpaque(false);
        summary.setOpaque(false);

        nick.setForeground(PRIMARY_COLOR);
        mark.setForeground(PRIMARY_COLOR);
        summary.setForeground(SECONDARY_COLOR);

        Font font = summary.getFont();
        summary.setFont(font.deriveFont(font.getSize() - 1F));

        avatar.setBorder(
                new RolloverBorder(
                        BorderFactory.createCompoundBorder(
                                BorderFactory.createLineBorder(new Color(0xCCCCCC), 1),
                                BorderFactory.createEmptyBorder(1, 1, 1, 1)
                        ),
                        BorderFactory.createCompoundBorder(
                                new LineBorder(new Color(96, 200, 253), 1, true),
                                new LineBorder(new Color(51, 139, 192), 1, true)
                        )
                )
        );
    }

    void initDataAndRenderer(Peer peer) {
        if (this.peer != null) {
            this.peer.removePropertyChangeListener(monitor);
        }
        this.peer = peer;

        if (peer != null) {
            peer.addPropertyChangeListener(monitor);
            setAvatarIcon(peer.getBigAvatar());
            setNickText(peer.getName());
            setMarkText(peer.getMark());
            setSummaryText(peer.getSummary());
        }

        renderer();
    }

    private void renderer() {
        nick.setForeground(PRIMARY_COLOR);

        this.add(avatar, new GridBagConstraints(0, 0, 1, 2, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(2, 0, 1, 5), 0, 0));
        if (!markIsEmpty()) {
            this.add(mark, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(0, 5, 1, -4), 0, 0));
            nick.setForeground(SECONDARY_COLOR);
        }
        this.add(nick, new GridBagConstraints(2, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(0, 5, 1, 2), 0, 0));
        this.add(summary, new GridBagConstraints(1, 1, 2, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(1, 5, 1, 2), 0, 0));
    }

    private boolean markIsEmpty() {
        return mark.getText() == null || mark.getText().length() < 1;
    }

    public void setAvatarIcon(Icon icon) {
        if (icon != null) {
            Image resize = ImageUtil.resize(((ImageIcon) icon).getImage(), AVATAR_SIZE);
            icon = new ImageIcon(resize);
        }
        avatar.setIcon(icon);
    }

    public Icon getAvatarIcon() {
        return avatar.getIcon();
    }

    public void setNickText(String nick) {
        this.nick.setText(markIsEmpty() ? nick : "(" + nick + ")");
    }

    public String getNickText() {
        return nick.getText();
    }

    public void setMarkText(String mark) {
        this.mark.setText(mark);
        String n = this.nick.getText();
        if (n != null) {
            n = n.replaceAll("^\\(|\\)$", "");
            this.nick.setText(markIsEmpty() ? n : "(" + n + ")");
        }
    }

    public String getMarkText() {
        return mark.getText();
    }

    public void setSummaryText(String additional) {
        this.summary.setText(additional);
    }

    public String getSummarylText() {
        return summary.getText();
    }

    private class DataChangeMonitor implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            initDataAndRenderer(peer);
        }
    }
}
