/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vacoor.xqq.ui.login;

import org.vacoor.xqq.core.bean.Status;
import org.vacoor.xqq.core.util.ImageResources;
import org.vacoor.xqq.ui.comp.combo.IconifyComboBox;
import org.vacoor.xqq.ui.comp.combo.IconifyComboItem;
import org.vacoor.xqq.ui.comp.combo.SimpleIconifyComboItem;
import org.vacoor.xqq.ui.comp.textfield.IconifyPasswdField;

import javax.swing.*;
import javax.swing.plaf.basic.BasicMenuBarUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


/**
 * 登陆面板
 * @author Vacoor
 */
public class LoginPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    private static final String LOGIN_ACTION_KEY = "action.login";
    private static final Dimension DEFAULT_PREFERRED_INPUT_SIZE = new Dimension(190, 35);

    private Status status = Status.ONLINE;
    private JMenu statusMenu = new JMenu(new StatusAction(status));
    private IconifyComboBox usernameCombo;
    private IconifyPasswdField passwdField;
    private Action loginAction;

    public LoginPanel() {
        this(new IconifyComboItem[]{
                new SimpleIconifyComboItem("2214963100", "Test", ImageResources.getIcon("images/head/002.png")),
                new SimpleIconifyComboItem("2213328508", "HelloWorld", ImageResources.getIcon("images/head/003.png")),
                new SimpleIconifyComboItem("272451590", "Vacoor", null)
        });
    }

    public LoginPanel(IconifyComboItem<?>[] items) {
        usernameCombo = new IconifyComboBox(new DefaultComboBoxModel(items));
        passwdField = new IconifyPasswdField();

        usernameCombo.setPreferredSize(DEFAULT_PREFERRED_INPUT_SIZE);
        passwdField.setPreferredSize(DEFAULT_PREFERRED_INPUT_SIZE);

        this.setLayout(new GridLayout(3, 1, 0, 10));
//        this.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.setBackground(Color.WHITE);

        statusMenu.setIcon(new ImageIcon(LoginPanel.class.getClassLoader().getResource("images/status/online.png")));

        for (Status status : Status.values()) {
            if (Status.OFFLINE == status) {
                continue;
            }
            statusMenu.add(new JMenuItem(new StatusAction(status)));
        }

        JMenuBar mb = new JMenuBar();
        mb.setUI(new BasicMenuBarUI());
        mb.setBorderPainted(false);
        mb.setOpaque(false);
        mb.add(statusMenu);

        this.add(mb);
        this.add(usernameCombo);
        this.add(passwdField);

        passwdField.getPasswdField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (loginAction != null && KeyEvent.VK_ENTER == e.getKeyCode()) {
                    loginAction.actionPerformed(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, ""));
                }
            }
        });
    }

    public String getUsername() {
        return String.valueOf(usernameCombo.getSelectedItem());
    }

    public char[] getPasswd() {
        return passwdField.getPassword();
    }

    public Status getStatus() {
        return status;
    }

    public void clearUsername() {
        usernameCombo.setSelectedItem("");
    }

    public void clearPassword() {
        passwdField.clear();
    }

    public Action getLoginAction() {
        return loginAction;
    }

    public void setLoginAction(Action action) {
        this.getActionMap().put(LOGIN_ACTION_KEY, action);
        this.loginAction = action;
    }

    private class StatusAction extends AbstractAction {
        private final Status status;

        public StatusAction(Status status) {
            super(status.getDesc(), status.getIcon());
            this.status = status;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            statusMenu.setAction(this);
            LoginPanel.this.status = status;
        }

        public Status getStatus() {
            return status;
        }
    }
}
