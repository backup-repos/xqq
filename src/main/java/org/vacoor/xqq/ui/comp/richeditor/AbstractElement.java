package org.vacoor.xqq.ui.comp.richeditor;

import javax.swing.*;

/**
 * @author: vacoor
 */
public abstract class AbstractElement implements RichElement {
    protected Style style = new Style();

    @Override
    public void appendTo(RichEditor richEditor) {
        insertTo(richEditor, richEditor.getContentLength());
        richEditor.moveCaretPositionToEnd();
    }

    @Override
    public void insertTo(RichEditor richEditor, int offset) {
        try {
            doInsert(richEditor.getTextPane(), offset);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract void doInsert(JTextPane textPane, int offset) throws Exception;

    public void setStyle(Style style) {
        this.style = style;
    }

    public Style getStyle() {
        return this.style;
    }
}
