package org.vacoor.xqq.ui.comp.tree;


import org.vacoor.xqq.core.bean.Buddy;
import org.vacoor.xqq.ui.comp.tooltip.TooltipWindow;

import javax.swing.*;

/**
 * 好友信息 Tooltip
 * <p/>
 *
 * @author: Vacoor
 */
public class BuddyInfoTooltip extends TooltipWindow {
    private JLabel label;

    public BuddyInfoTooltip() {
        label = new JLabel("<html><h1>感谢使用 X-QQ !!</h1></html>", JLabel.CENTER);
        label.setOpaque(false);
    }

    @Override
    protected JComponent getDisplayComponent() {
        return label;
    }

    public void setBuddy(Buddy buddy) {
//        label.setIcon(buddy.getBigAvatar());
        label.setText("<html><body>" +
                "UIN: " + buddy.getUin() + "<br />" +
                "QQ: " + buddy.getNo() + "<br />" +
                "昵称: " + buddy.getNick() + "<br />" +
                "备注: " + buddy.getMark() + "<br />" +
                "状态: " + buddy.getStatus().getDesc() + "<br />" +
                "个性签名: " + buddy.getSummary() + "<br />" +
                "</body></html>");
    }
}
