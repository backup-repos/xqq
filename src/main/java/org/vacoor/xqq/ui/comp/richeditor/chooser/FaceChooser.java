package org.vacoor.xqq.ui.comp.richeditor.chooser;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import java.awt.*;
import java.awt.event.*;

/**
 * User: vacoor
 */
public class FaceChooser extends JWindow {

    // 每行多少个表情
    private static final int COLUMNS = 15;
    private static final int ROWS = (int) Math.ceil(FaceUtil.TRANSFER_TABLE_FILE.length / 1.0 / COLUMNS);

    public static final String OK_COMMOND = FaceChooser.class.getName() + ".ok";
    public static final String CANCEL_COMMOND = FaceChooser.class.getName() + ".cancel";

    protected ActionListener listener;

    public FaceChooser() {
        this(null);
    }

    public FaceChooser(Window owner) {
        super(owner);
        init();
    }

    private void init() {
        JPanel facePanel = new JPanel(new GridLayout(ROWS, COLUMNS, 1, 1));

        Color borderColor = new Color(0xCCCCCC);
        CompoundBorder border = BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(30, 15, 30, 15, Color.WHITE),
                BorderFactory.createLineBorder(borderColor, 1)
        );

        facePanel.setBorder(border);
        facePanel.setBackground(borderColor);

        // 加载表情
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLUMNS; col++) {
                final int idx = COLUMNS * row + col;

                if (idx > FaceUtil.TRANSFER_TABLE_FILE.length - 1) {
                    break;
                }

                int faceID = FaceUtil.TRANSFER_TABLE[idx];

                final Icon staticIcon = FaceUtil.faceID2StaticFace(faceID);
                final Icon dynamicIcon = FaceUtil.faceID2DynamicFace(faceID);

                final JLabel face = new JLabel(staticIcon);

                face.setOpaque(true);
                face.setBackground(Color.WHITE);
                face.setToolTipText("简码: /" + FaceUtil.TRANSFER_TABLE_BRIEF_CODE[idx] + " , ID: " + faceID);
                face.setPreferredSize(new Dimension(30, 30));

                face.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        face.setBorder(BorderFactory.createLineBorder(new Color(0x5989FF), 1));
                        face.setIcon(dynamicIcon);
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        face.setBorder(null);
                        face.setIcon(staticIcon);
                    }

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (SwingUtilities.isLeftMouseButton(e) && listener != null) {
                            listener.actionPerformed(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, OK_COMMOND));
                        }
                        FaceChooser.this.setVisible(false);
                    }
                });

                facePanel.add(face);
            }

             /*- 移动到外部控制 */
            // 当鼠标点击当前窗口之外的地方, 则触发取消, 隐藏当前窗口
            Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
                @Override
                public void eventDispatched(AWTEvent event) {
                    Object source = event.getSource();

                    // 如果不是在组件上 / 不是鼠标释放 / 当前是不可见
                    if (!(source instanceof Component) || MouseEvent.MOUSE_RELEASED != event.getID() || !FaceChooser.this.isVisible()) {
                        return;
                    }

                    Component c = (Component) source;
                    /*
                    Point evtPoint = ((MouseEvent) event).getLocationOnScreen();
                    Rectangle bounds = FaceChooser.this.getBounds();
                    bounds.setLocation(FaceChooser.this.getLocationOnScreen());

                    //  不在面板范围, 或不是包含的组件上
                    if (!bounds.contains(evtPoint) || !containsComponent(FaceChooser.this, c)) {
                    */
                    // 如果不是不是包含的组件
                    if (!containsComponent(FaceChooser.this, c)) {
                        FaceChooser.this.setVisible(false);
                        listener.actionPerformed(new ActionEvent(event.getSource(), ActionEvent.ACTION_PERFORMED, CANCEL_COMMOND));
                    }
                }
            }, AWTEvent.MOUSE_EVENT_MASK);
        }

        this.add(facePanel);
        this.pack();
    }

    static boolean containsComponent(Component parent, Component child) {
        if (parent == null || child == null) {
            return false;
        }
        for (Component p = child; p != null; p = p.getParent()) {
            if (p == parent) {
                return true;
            }
        }
        return false;
    }

    public synchronized void addActionListener(ActionListener listener) {
        this.listener = AWTEventMulticaster.add(this.listener, listener);
    }

    public synchronized void removeActionListener(ActionListener listener) {
        this.listener = AWTEventMulticaster.remove(this.listener, listener);
    }
}
