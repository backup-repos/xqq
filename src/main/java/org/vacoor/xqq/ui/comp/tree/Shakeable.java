package org.vacoor.xqq.ui.comp.tree;

/**
 * TreeCellRenderer 渲染时是无状态的(共用一个 Renderer), 因此将状态数据单独抽取出来
 * <p/>
 * @author: Vacoor
 */
public interface Shakeable {
    public int TOP = 1 << 0;
    public int RIGHT = 1 << 1;
    public int BOTTOM = 1 << 2;
    public int LEFT = 1 << 3;
    public int HIDDEN = 1 << 4;
    public int NORMAL = TOP | RIGHT | BOTTOM | LEFT;

    void setShaking(boolean shaking);

    /**
     * 是否 Shaking
     */
    boolean isShaking();

    /**
     * 下一个 Shake 状态
     */
    int nextShakeStatus();
}
