package org.vacoor.xqq.ui.comp.richeditor.chooser;

import org.vacoor.nothing.ui.util.UIs;
import org.vacoor.xqq.core.util.ImageResources;

import javax.swing.*;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Locale;

/**
 * @author: vacoor
 */
public class StyleChooser extends JPanel {
    private static final Icon BOLD_ICON = ImageResources.getIcon("images/midtoolbar/ext_font/bold.png");
    private static final Icon ITALIC_ICON = ImageResources.getIcon("images/midtoolbar/ext_font/italic.png");
    private static final Icon UNDERLINE_ICON = ImageResources.getIcon("images/midtoolbar/ext_font/underline.png");
    private static final Icon COLOR_ICON = ImageResources.getIcon("images/midtoolbar/ext_font/color.png");

    public static final String FONT_FAMILY_PROPERTY_NAME = StyleChooser.class.getName() + ".fontFamily";
    public static final String FONT_SIZE_PROPERTY_NAME = StyleChooser.class.getName() + ".fontSize";
    public static final String BOLD_PROPERTY_NAME = StyleChooser.class.getName() + ".bold";
    public static final String ITALIC_PROPERTY_NAME = StyleChooser.class.getName() + ".italic";
    public static final String UNDERLINE_PROPERTY_NAME = StyleChooser.class.getName() + ".underline";
    public static final String COLOR_PROPERTY_NAME = StyleChooser.class.getName() + ".color";

    private static final Color[] COLORS = new Color[]{
            new Color(0xE5CFD9),
            new Color(0xBE86A1),
            new Color(0x7F3F56),
            new Color(0xECDFFE),
            new Color(0x7151D8),
            new Color(0x3228B8),
            new Color(0x99B2C7),
            new Color(0x39628C),
            new Color(0x526B9E),
            new Color(0x40C1),
            new Color(0x73E5),
            new Color(0x5DAFFB),
            new Color(0x1961EB),
            new Color(0xEC2FF),
            new Color(0x99F7FF),
            new Color(0x68F4FF),
            new Color(0x6FD5D9),
            new Color(0x1C8784),
            new Color(0x9BDFC4),
            new Color(0x2A474),
            new Color(0x9CCA82),
            new Color(0xC8CC7A),
            new Color(0x9DA900),
            new Color(0x6B680D),
            new Color(0x946102),
            new Color(0xEAD999),
            new Color(0xD19543),
            new Color(0x763B0A),
            new Color(0x464646),
            new Color(0x787878),
            new Color(0xDCDCDC),
            new Color(0xFFFFFF),
            new Color(0x0)
    };

    private static final Font BEST_FONT = UIs.getBestFont();

    static {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        Style ds = sc.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setFontFamily(ds, BEST_FONT.getFamily());
        StyleConstants.setFontSize(ds, BEST_FONT.getSize());
    }

    // 允许的最大和最小字体
    public static final int FONT_MIN_SIZE = 9;
    public static final int FONT_MAX_SIZE = 22;

    private String fontFamily;
    private int fontSize;
    private boolean bold;
    private boolean italic;
    private boolean underline;
    private Color color = Color.BLACK;

    private JComboBox fontFamilyCombo;
    private JComboBox fontSizeCombo;
    private JToggleButton boldBtn;
    private JToggleButton italicBtn;
    private JToggleButton underlineBtn;
    private JButton colorBtn;
    protected Handler handler;

    public StyleChooser() {
        init();
    }

    private void init() {
        fontFamilyCombo = new JComboBox(new DefaultComboBoxModel(getAvailableFontFamily()));
        fontSizeCombo = new JComboBox(new DefaultComboBoxModel(getAvailableFontSize()));
        boldBtn = new JToggleButton(BOLD_ICON, false);
        italicBtn = new JToggleButton(ITALIC_ICON, false);
        underlineBtn = new JToggleButton(UNDERLINE_ICON, false);
        colorBtn = new JButton(COLOR_ICON);

        this.setLayout(new FlowLayout(FlowLayout.LEADING));
        this.add(fontFamilyCombo);
        this.add(fontSizeCombo);
        this.add(boldBtn);
        this.add(italicBtn);
        this.add(underlineBtn);
        this.add(colorBtn);

        fontFamilyCombo.setBorder(null);
        fontFamilyCombo.setPreferredSize(new Dimension(150, 20));

        fontSizeCombo.setBorder(null);
        fontSizeCombo.setPreferredSize(new Dimension(50, 20));

        boldBtn.setBorder(null);
        boldBtn.setPreferredSize(new Dimension(20, 20));

        italicBtn.setBorder(null);
        italicBtn.setPreferredSize(new Dimension(20, 20));

        underlineBtn.setBorder(null);
        underlineBtn.setPreferredSize(new Dimension(20, 20));

        colorBtn.setBorder(null);
        colorBtn.setPreferredSize(new Dimension(20, 20));

        this.setBorder(null);
        this.setBackground(new Color(0xD3E8F3));
//        this.setPreferredSize(new Dimension(300, 25));

        handler = new Handler();
        fontFamilyCombo.addItemListener(handler);
        fontSizeCombo.addItemListener(handler);
        boldBtn.addItemListener(handler);
        italicBtn.addItemListener(handler);
        underlineBtn.addItemListener(handler);
        colorBtn.addActionListener(handler);

        initDefaults();
    }

    private void initDefaults() {
//        fontFamilyCombo.setSelectedItem(style.getFontFamily());
//        fontSizeCombo.setSelectedItem(style.getFontSize());
//        boldBtn.setSelected(style.isBold());
//        italicBtn.setSelected(style.isItalic());
//        underlineBtn.setSelected(style.isUnderline());
        fontFamilyCombo.setSelectedItem(BEST_FONT.getFamily());
        fontSizeCombo.setSelectedItem(BEST_FONT.getSize());
    }

    /**
     * 获取系统的有效字体
     *
     * @return
     */
    public static String[] getAvailableFontFamily() {
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames(Locale.CHINA);
    }

    /**
     * 获取有效的字体大小 (该方法暂时固定返回 {@link #FONT_MIN_SIZE} ~ {@link #FONT_MAX_SIZE}
     *
     * @return
     */
    public static Integer[] getAvailableFontSize() {
        Integer[] result = new Integer[FONT_MAX_SIZE + 1 - FONT_MIN_SIZE];

        for (int i = 0; i < result.length; i++) {
            result[i] = FONT_MIN_SIZE + i;
        }
        return result;
    }

    //
    protected class Handler implements ItemListener, ActionListener {

        @Override
        public void itemStateChanged(ItemEvent e) {
            if (fontFamilyCombo == e.getSource() && ItemEvent.SELECTED == e.getStateChange()) {
                String oldValue = fontFamily;
                fontFamily = (String) e.getItem();
                if (!fontFamily.equals(oldValue)) {
                    firePropertyChange(FONT_FAMILY_PROPERTY_NAME, oldValue, fontFamily);
                }
                return;
            }

            if (fontSizeCombo == e.getSource() && ItemEvent.SELECTED == e.getStateChange()) {
                int oldValue = fontSize;
                fontSize = (Integer) e.getItem();
                if (oldValue != fontSize) {
                    firePropertyChange(FONT_SIZE_PROPERTY_NAME, oldValue, fontSize);
                }
                return;
            }

            if (boldBtn == e.getSource()) {
                boolean oldValue = bold;
                if (oldValue != (bold = ItemEvent.SELECTED == e.getStateChange())) {
                    firePropertyChange(BOLD_PROPERTY_NAME, oldValue, bold);
                }
                return;
            }

            if (italicBtn == e.getSource()) {
                boolean oldValue = italic;
                if (oldValue != (italic = ItemEvent.SELECTED == e.getStateChange())) {
                    firePropertyChange(ITALIC_PROPERTY_NAME, oldValue, italic);
                }
                return;
            }

            if (underlineBtn == e.getSource()) {
                boolean oldValue = underline;
                if (oldValue != (underline = ItemEvent.SELECTED == e.getStateChange())) {
                    firePropertyChange(UNDERLINE_PROPERTY_NAME, oldValue, underline);
                }
                return;
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (colorBtn == e.getSource()) {
                Color oldColor = color;
                Color newColor = JColorChooser.showDialog(colorBtn, "不想搞,颜色选择没必要呀", color);
                if (newColor != null && !newColor.equals(oldColor)) {
                    color = newColor;
                    firePropertyChange(COLOR_PROPERTY_NAME, oldColor, newColor);
                }
            }
        }
    }
}
