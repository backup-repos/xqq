package org.vacoor.xqq.ui.comp.richeditor;

import javax.swing.*;
import javax.swing.text.*;

/**
 * User: vacoor
 */

public class Face extends AbstractElement {
    public static final String FACE_ID_ATTR_KEY = Face.class.getName() + ".ID";
    public static final String FACE_TEXT_CONTENT = " ";

    private int id;
    private Icon icon;

    public Face(int id, Icon icon) {
        this.id = id;
        this.icon = icon;
    }

    @Override
    protected void doInsert(JTextPane textPane, int offset) throws Exception {
        StyleConstants.setIcon(style, icon);
        style.addAttribute(FACE_ID_ATTR_KEY, id);
        textPane.getStyledDocument().insertString(offset, FACE_TEXT_CONTENT, style);
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "[face" + id +"]";
    }
}
