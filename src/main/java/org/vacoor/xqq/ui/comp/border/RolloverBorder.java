package org.vacoor.xqq.ui.comp.border;

import javax.swing.*;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ArrayList;

/**
 * User: Vacoor
 */
public class RolloverBorder extends AbstractBorder {
    private Handler handler;
    private Border normalBorder;
    private Border rolloverBorder;
    private Component component;
    private List<Component> triggers = new ArrayList<Component>();

    public RolloverBorder(Border normalBorder, Border rolloverBorder) {
        this.normalBorder = normalBorder;
        this.rolloverBorder = rolloverBorder;
        handler = new Handler();
    }

    /**
     * 设置触发对象, 允许鼠标在另一个组件上的动作来触发当前组件的边框
     */
    public void setTrigger(Component trigger) {
        if (!triggers.isEmpty()) {
            for (Component tri : triggers) {
                removeTrigger(tri);
            }
        }
        if (trigger != null) {
            addTrigger(trigger);
        }
    }

    public void addTrigger(Component... triggers) {
        for (Component trigger : triggers) {
            trigger.addMouseListener(handler);
            this.triggers.add(trigger);
        }
    }

    public void removeTrigger(Component... triggers) {
        if (triggers == null) {
            return;
        }
        for (Component trigger : triggers) {
            trigger.removeMouseListener(handler);
        }
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        if (triggers.isEmpty()) {
            setTrigger(c);
        }
        this.component = c;
        if (c instanceof JComponent) {
            ((JComponent) c).setBorder(normalBorder);
        }
    }

    @Override
    public Insets getBorderInsets(Component c) {
        Insets insets = new Insets(0, 0, 0, 0);
        Insets nInsets = normalBorder.getBorderInsets(c);
        Insets rInsets = rolloverBorder.getBorderInsets(c);
        insets.top = Math.max(nInsets.top, rInsets.top);
        insets.left = Math.max(nInsets.left, rInsets.left);
        insets.bottom = Math.max(nInsets.bottom, rInsets.bottom);
        insets.right = Math.max(nInsets.right, rInsets.right);
        return insets;
    }

    @Override
    public Insets getBorderInsets(Component c, Insets insets) {
        Insets nInsets = normalBorder.getBorderInsets(c);
        Insets rInsets = rolloverBorder.getBorderInsets(c);
        insets.top = Math.max(nInsets.top, rInsets.top);
        insets.left = Math.max(nInsets.left, rInsets.left);
        insets.bottom = Math.max(nInsets.bottom, rInsets.bottom);
        insets.right = Math.max(nInsets.right, rInsets.right);
        return insets;
    }

    private class Handler extends MouseAdapter {
        @Override
        public void mouseEntered(MouseEvent e) {
//            Component component = e.getComponent();
            if (component instanceof JComponent) {
                ((JComponent) component).setBorder(rolloverBorder);
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
//            Component component = e.getComponent();
            if (component instanceof JComponent) {
                ((JComponent) component).setBorder(normalBorder);
            }
        }
    }
}
