package org.vacoor.xqq.ui.comp.tree;

import org.vacoor.xqq.core.bean.IdentifiableAndObservableData;

/**
 * 该类中方法不派发 PropertyChange 事件, 需要自己调用 {@link javax.swing.JTree#repaint(java.awt.Rectangle)}
 * 1. {@link javax.swing.tree.DefaultTreeModel#nodeChanged(javax.swing.tree.TreeNode)} 效率太低
 * 2. 会造成死循环更新(主要是 nextShakeState)
 * 3. 一般数据没有变化, 仅仅是显示变化
 * <p/>
 *
 * @author: Vacoor
 */
public abstract class ShakeableNode<D extends IdentifiableAndObservableData> extends IdentifiableAndObservableDataNode<D> implements Shakeable {
    protected boolean shaking = false;
    protected int currentShakeStatus = NORMAL;
    private long lastChangeTime;

    public ShakeableNode(long id) {
        super(id);
    }

    public ShakeableNode(D data) {
        super(data);
    }

    protected ShakeableNode(long id, D data) {
        super(id, data);
    }

    @Override
    public void setShaking(boolean shaking) {
        this.shaking = shaking;
    }

    @Override
    public boolean isShaking() {
        return shaking;
    }

    @Override
    public synchronized int nextShakeStatus() {
        if (!isShaking()) {
            return NORMAL;
        }
        // 树可能会频繁更新, 保证在调用频率过高时候不会更新过快, 应该和定时器保持一致
        long l = System.currentTimeMillis();
        if (l - lastChangeTime > 100) {
            currentShakeStatus = doNextShakeStatus();
            lastChangeTime = l;
        }
        return currentShakeStatus;
    }

    protected abstract int doNextShakeStatus();
}
