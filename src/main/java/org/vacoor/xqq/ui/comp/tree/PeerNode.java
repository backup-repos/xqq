package org.vacoor.xqq.ui.comp.tree;

import org.vacoor.xqq.core.bean.Peer;

/**
 * @author: Vacoor
 */
public class PeerNode<T extends Peer> extends ShakeableNode<T> {

    public PeerNode(T peer) {
        super(peer);
    }

    public PeerNode(long id, T data) {
        super(id, data);
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public T getData() {
        return super.getData();
    }

    private static final int[] SHAKE_STATUS = new int[]{LEFT + BOTTOM, TOP, RIGHT + BOTTOM, TOP};
    private int shakeIdx;

    @Override
    protected int doNextShakeStatus() {
        if (shakeIdx > SHAKE_STATUS.length - 1) {
            shakeIdx = 0;
        }
        return SHAKE_STATUS[shakeIdx++];
    }

    @Override
    public String toString() {
        return userObject == null ? "empty buddy node" : ((Peer) userObject).getName();
    }
}
