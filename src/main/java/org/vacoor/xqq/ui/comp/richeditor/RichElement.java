package org.vacoor.xqq.ui.comp.richeditor;

import java.io.Serializable;

/**
 * User: vacoor
 */
public interface RichElement extends Serializable {

    Style getStyle();

    void setStyle(Style style);

    void appendTo(RichEditor richEditor);

    void insertTo(RichEditor richEditor, int offset);
}
