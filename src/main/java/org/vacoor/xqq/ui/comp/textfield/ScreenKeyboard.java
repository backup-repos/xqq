/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vacoor.xqq.ui.comp.textfield;

import org.vacoor.xqq.core.util.ImageResources;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Vacoor
 */
public class ScreenKeyboard extends JDialog {
    private static final long serialVersionUID = 1L;

    private static final Image SCREEN_KEYBOARD_ICON = ImageResources.getImage("images/keyboard/screenkeyboard.png");
    private static final Dimension SIZE = new Dimension(SCREEN_KEYBOARD_ICON.getWidth(null), SCREEN_KEYBOARD_ICON.getHeight(null));
    private JPanel panel;

    public ScreenKeyboard() {
        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(SCREEN_KEYBOARD_ICON, 0, 0, this);
            }
        };
        panel.setPreferredSize(SIZE);
        panel.setMinimumSize(SIZE);
        this.add(panel);
        this.setUndecorated(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setAlwaysOnTop(true);
        panel.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                setVisible(false);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                }
                setVisible(false);
            }
        });
    }

    public void showScrKeyboard(int x, int y) {
        this.setLocation(x, y);
        this.setVisible(true);
    }
}
