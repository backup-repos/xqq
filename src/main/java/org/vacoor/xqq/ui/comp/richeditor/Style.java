package org.vacoor.xqq.ui.comp.richeditor;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.*;

/**
 * 基本的输入样式
 * User: Vacoor
 */
public class Style extends SimpleAttributeSet implements javax.swing.text.Style {

    private EventListenerList listenerList = new EventListenerList();
    private ChangeEvent changeEvent;
    private String name;

    /**
     * 基于 {@link javax.swing.text.StyleContext#DEFAULT_STYLE} 创建默认样式
     */
    public Style() {
        this(StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE));
    }

    /**
     * 基于 {@link javax.swing.text.AttributeSet} 创建样式(默认构造时,只取简单样式)
     *
     * @param attrSet 包含样式属性的 AttributeSet
     */
    public Style(AttributeSet attrSet) {
        this(
                StyleConstants.getFontFamily(attrSet),
                StyleConstants.getFontSize(attrSet),
                StyleConstants.isBold(attrSet),
                StyleConstants.isItalic(attrSet),
                StyleConstants.isUnderline(attrSet),
                StyleConstants.getForeground(attrSet)
        );
    }

    public Style(String fontFamily, int fontSize, boolean bold, boolean italic, boolean underline, Color color) {
        setFontFamily(fontFamily);
        setFontSize(fontSize);
        setBold(bold);
        setItalic(italic);
        setUnderline(underline);
        setColor(color);
    }

    public String getFontFamily() {
        return StyleConstants.getFontFamily(this);
    }

    public void setFontFamily(String fontFamily) {
        StyleConstants.setFontFamily(this, fontFamily);
    }

    public int getFontSize() {
        return StyleConstants.getFontSize(this);
    }

    public void setFontSize(int fontSize) {
        StyleConstants.setFontSize(this, fontSize);
    }

    public boolean isBold() {
        return StyleConstants.isBold(this);
    }

    public void setBold(boolean bold) {
        StyleConstants.setBold(this, bold);
    }

    public boolean isItalic() {
        return StyleConstants.isItalic(this);
    }

    public void setItalic(boolean italic) {
        StyleConstants.setItalic(this, italic);
    }

    public boolean isUnderline() {
        return StyleConstants.isUnderline(this);
    }

    public void setUnderline(boolean underline) {
        StyleConstants.setUnderline(this, underline);
    }

    public Color getColor() {
        return StyleConstants.getForeground(this);
    }

    public void setColor(Color color) {
        StyleConstants.setForeground(this, color);
    }

    @Override
    public void addAttribute(Object name, Object value) {
        super.addAttribute(name, value);
        fireStateChanged();
    }

    @Override
    public void addAttributes(AttributeSet attributes) {
        super.addAttributes(attributes);
        fireStateChanged();
    }

    protected void fireStateChanged() {
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i]==ChangeListener.class) {
                // Lazily create the event:
                if (changeEvent == null)
                    changeEvent = new ChangeEvent(this);
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addChangeListener(ChangeListener l) {
        listenerList.add(ChangeListener.class, l);
    }

    public void removeChangeListener(ChangeListener l) {
        listenerList.remove(ChangeListener.class, l);
    }
}
