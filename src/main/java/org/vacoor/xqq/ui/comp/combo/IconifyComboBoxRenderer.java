package org.vacoor.xqq.ui.comp.combo;

import org.vacoor.xqq.core.util.ImageResources;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * 本来是准备替代 {@link org.vacoor.xqq.ui.comp.combo.IconifyComboBoxPopup}
 */
public class IconifyComboBoxRenderer extends BasicComboBoxRenderer {
    private static final ImageIcon CLOSE_ICON = ImageResources.getIcon("images/button/x.png");

    private JPanel smallPanel;
    private JPanel bigPanel;

    private JLabel id;
    private JLabel nick;
    private JLabel icon;
    private JLabel close;

    public IconifyComboBoxRenderer() {
        smallPanel = new JPanel(new GridBagLayout());
        bigPanel = new JPanel(new GridBagLayout());

        id = new JLabel();
        nick = new JLabel();
        icon = new JLabel();
        close = new JLabel(CLOSE_ICON);

        smallPanel.setBackground(Color.WHITE);
        smallPanel.setPreferredSize(new Dimension(180, 25));

        bigPanel.setBackground(new Color(55, 143, 207));
        bigPanel.add(icon, new GridBagConstraints(0, 0, 1, 2, 0, 1, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(2, 2, 2, 2), 0, 0));
        bigPanel.add(nick, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.BASELINE_LEADING, GridBagConstraints.CENTER, new Insets(3, 2, 1, 0), 1, 0));
        bigPanel.add(id, new GridBagConstraints(1, 1, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(1, 2, 3, 0), 1, 0));
        bigPanel.add(close, new GridBagConstraints(2, 0, 1, 2, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.CENTER, new Insets(0, 0, 0, 10), 0, 0));
        bigPanel.setPreferredSize(new Dimension(180, 45));

        close.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                close.setBorder(BorderFactory.createLineBorder(new Color(167, 195, 212)));
            }
        });
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean selected, boolean cellHasFocus) {
        if (!(value instanceof IconifyComboItem)) {
            return super.getListCellRendererComponent(list, value, index, selected, cellHasFocus);
        }

        IconifyComboItem<?> item = (IconifyComboItem<?>) value;

        JPanel renderPanel;
        id.setText(String.valueOf(item.getId()));
        nick.setText(item.getNick());
        icon.setIcon(item.getIcon());
        if (!selected) {
            smallPanel.add(icon, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 1, 1));
            smallPanel.add(id, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(0, 0, 0, 0), 1, 1));
            renderPanel = smallPanel;
        } else {
            bigPanel.add(icon, new GridBagConstraints(0, 0, 1, 2, 0, 1, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(2, 2, 2, 2), 0, 0));
            bigPanel.add(id, new GridBagConstraints(1, 1, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.CENTER, new Insets(1, 2, 3, 0), 1, 0));
            renderPanel = bigPanel;
        }
        return renderPanel;
    }

}
