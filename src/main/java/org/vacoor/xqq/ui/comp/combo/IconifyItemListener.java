/*
 * MItemListener.java
 * 自定义的接口
 */
package org.vacoor.xqq.ui.comp.combo;

import java.util.EventListener;

public interface IconifyItemListener extends EventListener {

    public boolean itemDeleting(IconifyComboItem item);

    public void itemDeleted(IconifyComboItem item);

    public void itemSelected(IconifyComboItem item);
}
