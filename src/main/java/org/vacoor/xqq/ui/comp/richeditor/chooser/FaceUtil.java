package org.vacoor.xqq.ui.comp.richeditor.chooser;

import org.vacoor.xqq.core.util.ImageResources;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * User: vacoor
 */
public abstract class FaceUtil {

    private FaceUtil() {
    }

    // 显示索引 --> 表情代码 转换表 (取自Web QQ) 15 * 9
    public static final int[] TRANSFER_TABLE = {
            14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0,
            50, 51, 96, 53, 54, 73, 74, 75, 76, 77, 78, 55, 56, 57, 58,
            79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 97, 98, 99, 100,
            101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 32, 113, 114,
            115, 63, 64, 59, 33, 34, 116, 36, 37, 38, 91, 92, 93, 29, 117,
            72, 45, 42, 39, 62, 46, 47, 71, 95, 118, 119, 120, 121, 122, 123,
            124, 27, 21, 23, 25, 26, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,

            52, 24, 22, 20, 60, 61, 89, 90, 31, 94, 65, 35, 66, 67, 68,
            69, 70, 15, 16, 17, 18, 19, 28, 30, 40, 41, 43, 44, 48, 49
    };

    // 显示索引 --> 文件序号 转换表 15 * 7
    public static final int[] TRANSFER_TABLE_FILE = {
            14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0,
            15, 16, 96, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
            30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 97, 98, 99, 100, 101,
            102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 89, 113, 114, 115,
            60, 61, 46, 63, 64, 116, 66, 67, 53, 54, 55, 56, 57, 117, 59,
            75, 74, 69, 49, 76, 77, 78, 79, 118, 119, 120, 121, 122, 123, 124,
            42, 85, 43, 41, 86, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134
    };

    // 索引 --> 简码 15 * 5
    public static final String[] TRANSFER_TABLE_BRIEF_CODE = {
            "wx", "pz", "se", "fd", "dy", "ll", "hx", "bz", "shui", "dk", "gg", "fn", "tp", "cy", "jy",
            "ng", "ku", "lengh", "zk", "tuu", "tx", "ka", "baiy", "am", "jie", "kun", "jk", "lh", "hanx", "db",
            "fendou", "zhm", "yiw", "xu", "yun", "zhem", "shuai", "kl", "qiao", "cj", "ch", "kb", "gz", "qd", "huaix",
            "zhh", "yhh", "hq", "bs", "wq", "kk", "yx", "qq", "xia", "kel", "cd", "xig", "pj", "lq", "pp",
            "kf", "fan", "zt", "mg", "dx", "sa", "xin", "xs", "dg", "shd", "zhd", "dao", "zq", "pch", "bb",
            "yl", "ty", "lw", "yb", "qiang", "ruo", "ws", "shl", "bq", "gy", "qt", "cj", "aini", "bu", "hd",
            "aiq", "fw", "tiao", "fd", "oh", "zhq", "kt", "ht", "tsh", "hsh", "jd", "jw", "xw", "zuotj", "youtj",
//        "shx", "bp", "dl", "facai", "kg", "gw", "youjian"
    };

    // 表情代码 --> 图标
    private static Map<Integer, Icon> staticFaces = new HashMap<Integer, Icon>();
    private static Map<Integer, Icon> dynamicFaces = new HashMap<Integer, Icon>();


    /**
     * 根据 FaceID 获取对应的显示索引
     *
     * @param id
     * @return
     */
    public static int faceID2Idx(int id) {
        for (int i = 0; i < TRANSFER_TABLE.length; i++) {
            if (TRANSFER_TABLE[i] == id) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 根据 FaceID 获取对应的文件序号
     *
     * @param id
     * @return
     */
    public static int faceID2FileSeq(int id) {
        int idx = faceID2Idx(id);
        if (-1 == idx) {
            return -1;
        }
        return TRANSFER_TABLE_FILE[idx];
    }

    /**
     * 根据 FaceID 获取对应的Icon
     *
     * @param id
     * @return
     */
    public static Icon faceID2StaticFace(int id) {
        Icon icon = staticFaces.get(id);
        if (icon != null) {
            return icon;
        }
        synchronized (staticFaces) {
            if (icon == null) {
                int fs = faceID2FileSeq(id);
                icon = new ImageIcon(
                        Thread.currentThread().getContextClassLoader().getResource("images/face/" + fs + "fix.bmp")
                );
                staticFaces.put(id, icon);
            }
        }

        return icon;
    }

    public static Icon faceID2DynamicFace(int id) {
        Icon icon = dynamicFaces.get(id);
        if (icon != null) {
            return icon;
        }
        synchronized (dynamicFaces) {
            if (icon == null) {
                int fs = faceID2FileSeq(id);
                icon = ImageResources.getIcon("images/face/" + fs + ".gif");
                dynamicFaces.put(id, icon);
            }
        }
        return icon;
    }

    public static int briefCode2Index(String briefCode) {
        for (int i = 0; i < TRANSFER_TABLE_BRIEF_CODE.length; i++) {
            if (TRANSFER_TABLE_BRIEF_CODE[i].equals(briefCode)) {
                return i;
            }
        }
        return -1;
    }
}
