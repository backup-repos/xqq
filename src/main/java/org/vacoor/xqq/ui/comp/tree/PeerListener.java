package org.vacoor.xqq.ui.comp.tree;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vacoor.xqq.core.bean.Buddy;
import org.vacoor.xqq.core.bean.Discussion;
import org.vacoor.xqq.core.bean.Group;
import org.vacoor.xqq.core.bean.Peer;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * 用于处理好友 Tooltip 和点击打开聊天窗口
 *
 * @author: vacoor
 */
public class PeerListener extends TreeNodeMouseAdapter {
    private static final Logger logger = LoggerFactory.getLogger(PeerListener.class);
    private static final long DELAY = 1000;

    private BuddyInfoTooltip tooltip = new BuddyInfoTooltip();

    public PeerListener() {
        init();
    }

    private void init() {
        tooltip.setSize(230, 160);
        tooltip.getContentPane().setBackground(new Color(200, 200, 200));
        tooltip.setVisible(false);
    }

    // ------- Tooltip 控制
    @Override
    public void mouseMovedOnNode(MouseEvent e, int row, Point point) {
        JTree tree = (JTree) e.getSource();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getPathForRow(row).getLastPathComponent();

        // 节点为 null, 或者不是叶节点 或 节点数据不是 PeerNode
        if (node == null || !node.isLeaf() || !(node instanceof PeerNode)) {
            return;
        }

        Peer data = ((PeerNode<?>) node).getData();
        if (!(data instanceof Buddy)) {
            return;
        }

        Buddy buddy = (Buddy) data;
        Rectangle bounds = tree.getRowBounds(row);

        // 事件触发区域
        Rectangle trigerBounds = new Rectangle(10, 5, bounds.height - 10, bounds.height - 10);

        if (trigerBounds.contains(point)) {
            int containerX = tree.getLocationOnScreen().x;
            int width = tree.getWidth();

            Window win = SwingUtilities.getWindowAncestor(tree);
            if (win != null) {
                containerX = win.getLocationOnScreen().x;
                width = win.getWidth();
            }

            int x = containerX - tooltip.getWidth() - 10;
            if (x < -20) {
                x = containerX + width + 10;
            }

            int y = (int) (tree.getLocationOnScreen().getY() + bounds.getY() - 20);

            if (!this.tooltip.isVisible()) {
                this.tooltip.setBuddy(buddy);
                this.tooltip.setLocation(x, y);
                this.tooltip.delayShow(DELAY);
            }
        } else {
            tooltip.cancelDelayShow();
            tooltip.setVisible(false);
        }
    }

    @Override
    public void mouseExitedOnNode(MouseEvent e, int row, Point point) {
        tooltip.cancelDelayShow();
        tooltip.setVisible(false);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        super.mouseExited(e);
        tooltip.cancelDelayShow();
        tooltip.setVisible(false);
    }

    // ----------- 打开聊天窗口
    @Override
    public void mouseClickedOnNode(MouseEvent e, TreeNode node) {
        if (node == null || !(node instanceof PeerNode) || !SwingUtilities.isLeftMouseButton(e) || e.getClickCount() < 2) {
            return;
        }
        PeerNode<?> n = (PeerNode<?>) node;
        Peer peer = n.getData();
        if (peer == null) {
            return;
        }

        if (peer instanceof Buddy) {
            mouseClickedOnBuddy((Buddy) peer);
        } else if (peer instanceof Discussion) {
            mouseClickedOnDiscussion((Discussion) peer);
        } else if (peer instanceof Group) {
            mouseClickedOnGroup((Group) peer);
        }
    }

    protected void mouseClickedOnBuddy(Buddy buddy) {
    }

    protected void mouseClickedOnDiscussion(Discussion discu) {
    }

    protected void mouseClickedOnGroup(Group group) {
    }
}
