package org.vacoor.xqq.ui.comp.combo;

import javax.swing.*;

/**
 */
public interface IconifyComboItem<ID> {

    ID getId();

    String getNick();

    Icon getIcon();

}
