package org.vacoor.xqq.ui.comp.richeditor;

import javax.swing.*;
import javax.swing.text.StyledDocument;

/**
 * @author: Vacoor
 */
public class Text extends AbstractElement {
    private String text;

    public Text(String text) {
        this.text = text;
    }

    @Override
    protected void doInsert(JTextPane textPane, int offset) throws Exception {
        StyledDocument doc = textPane.getStyledDocument();
        doc.insertString(offset, text, style);
    }

    @Override
    public String toString() {
        return text;
    }
}
