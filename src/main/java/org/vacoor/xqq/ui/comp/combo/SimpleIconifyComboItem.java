package org.vacoor.xqq.ui.comp.combo;

import javax.swing.*;

/**
 * @author Vacoor
 */
public class SimpleIconifyComboItem implements IconifyComboItem<String> {
    private String id;
    private String nick;
    private Icon icon;

    public SimpleIconifyComboItem(String id) {
        this.id = id;
    }

    public SimpleIconifyComboItem(String id, Icon icon) {
        this.id = id;
        this.icon = icon;
    }

    public SimpleIconifyComboItem(String id, String nick, Icon icon) {
        this.id = id;
        this.nick = nick;
        this.icon = icon;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleIconifyComboItem)) return false;

        SimpleIconifyComboItem that = (SimpleIconifyComboItem) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return id;
    }
}
