package org.vacoor.xqq.ui.comp.tabpane;

import javax.swing.*;

/**
 * 快速切换 TabPane
 *
 * @author: Vacoor
 */
public class QuickOpTabbedPane extends JTabbedPane {
    public static final int FILL_TAB_LAYOUT = 2;

    private int tabLayoutPolicy;
    private boolean enabledTabDBClickClose;
    private boolean enabledTabQuickSwitch;

    public QuickOpTabbedPane() {
        super();
    }

    public QuickOpTabbedPane(int tabPlacement) {
        super(tabPlacement);
    }

    public QuickOpTabbedPane(int tabPlacement, int tabLayoutPolicy) {
        super(tabPlacement, tabLayoutPolicy);
    }

    public int tabForCoordinate(int x, int y) {
        /**
         * 在 BasicTabbedpaneUI#tabForCoordinate 中压根就没用第一个参数...
         * {@link javax.swing.plaf.basic.BasicTabbedPaneUI#tabForCoordinate(javax.swing.JTabbedPane, int, int)}
         */
        return getUI().tabForCoordinate(this, x, y);
    }

    public void setEnabledTabDBClickClose(boolean enable) {
        this.enabledTabDBClickClose = enable;
    }

    public boolean isEnabledTabDBClickClose() {
        return enabledTabDBClickClose;
    }

    public boolean isEnabledTabQuickSwitch() {
        return enabledTabQuickSwitch;
    }

    /**
     * 启用快速切换标签页, Alt + 1 ~ 9 切换到标签页 1~9, Alt + P 切换到上一个, Alt + N 切换到下一个
     *
     * @param enabledTabQuickSwitch
     */
    public void setEnabledTabQuickSwitch(boolean enabledTabQuickSwitch) {
        this.enabledTabQuickSwitch = enabledTabQuickSwitch;
    }

    @Override
    public void setTabLayoutPolicy(int tabLayoutPolicy) {
        if (tabLayoutPolicy != WRAP_TAB_LAYOUT && tabLayoutPolicy != SCROLL_TAB_LAYOUT && tabLayoutPolicy != FILL_TAB_LAYOUT) {
            throw new IllegalArgumentException("illegal tab layout policy: must be WRAP_TAB_LAYOUT or SCROLL_TAB_LAYOUT");
        }
        if (this.tabLayoutPolicy != tabLayoutPolicy) {
            int oldValue = this.tabLayoutPolicy;
            this.tabLayoutPolicy = tabLayoutPolicy;
            firePropertyChange("tabLayoutPolicy", oldValue, tabLayoutPolicy);
            revalidate();
            repaint();
        }
    }

    @Override
    public int getTabLayoutPolicy() {
        return tabLayoutPolicy;
    }
}
