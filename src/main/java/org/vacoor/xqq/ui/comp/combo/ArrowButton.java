package org.vacoor.xqq.ui.comp.combo;

import org.vacoor.xqq.core.util.ImageResources;

import javax.swing.*;
import java.awt.*;

public class ArrowButton extends JButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private ImageIcon arrowIcon = ImageResources.getIcon("images/button/inputbtn_normal.png");
    private ImageIcon arrowRollIcon = ImageResources.getIcon("images/button/inputbtn_highlight.png");
    private ImageIcon arrowPressIcon = ImageResources.getIcon("images/button/inputbtn_down.png");

    public ArrowButton() {
        this.setBorder(null);
        this.setIcon(arrowIcon);
        this.setRolloverEnabled(true);
        this.setFocusPainted(false);    //不绘制焦点
        this.setContentAreaFilled(false); //不绘制组件区域
        this.setFocusable(false);
        this.setRequestFocusEnabled(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
//        g2d.clearRect(0, 0, getWidth(), getHeight());
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Image img = arrowIcon.getImage();

        if (this.getModel().isPressed()) {
            img = arrowPressIcon.getImage();
        } else if (this.isRolloverEnabled() && this.getModel().isRollover()) {
            img = arrowRollIcon.getImage();
        }
        g2d.drawImage(img, 0, 0, getWidth(), getHeight(), new Color(255, 255, 255, 0), null);
    }
}
