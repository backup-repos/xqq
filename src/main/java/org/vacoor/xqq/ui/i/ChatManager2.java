package org.vacoor.xqq.ui.i;

import org.vacoor.xqq.core.bean.Peer;

/**
 * Created by Administrator on 14-1-1.
 */
public interface ChatManager2 extends UserNotifier<Chat2> {

    Chat2 getChat(Peer peer, boolean create);

    Chat2 getCurrentChat();

    void setCurrentChat(Chat2 chat);

}
