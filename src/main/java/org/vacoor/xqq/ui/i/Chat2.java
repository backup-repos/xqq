package org.vacoor.xqq.ui.i;

import org.vacoor.xqq.core.bean.Peer;
import org.vacoor.xqq.core.poll.impl.AbstractPollReply;

/**
 * User: Vacoor
 */
public interface Chat2 {

    Peer getChatPeer();

    void writeMessage(AbstractPollReply msg);

}
