package org.vacoor.xqq.ui.i;

/**
 * User: Vacoor
 */
public interface UserNotifier<C> {

    void notifyUser(C c);

    void cancelNotify(C c);

}
