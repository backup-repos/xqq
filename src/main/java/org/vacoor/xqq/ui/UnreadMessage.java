package org.vacoor.xqq.ui;

import org.vacoor.xqq.core.bean.Peer;
import org.vacoor.xqq.core.poll.impl.AbstractPollReply;

import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

/**
 */
class UnreadMessage implements Iterable<AbstractPollReply> {
    private Vector<AbstractPollReply> messages = new Vector<AbstractPollReply>();
    private final Peer peer;

    public UnreadMessage(Peer peer) {
        this.peer = peer;
    }

    public UnreadMessage addMessage(AbstractPollReply msg) {
        messages.add(msg);
        return this;
    }

    @Override
    public Iterator<AbstractPollReply> iterator() {
        return Collections.unmodifiableList(messages).iterator();
    }
}
