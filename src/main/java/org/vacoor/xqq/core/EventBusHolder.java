package org.vacoor.xqq.core;

import com.google.common.eventbus.EventBus;

/**
 * User: vacoor
 */
public class EventBusHolder {
    public static EventBus eventBus = new EventBus("x-QQ");

    public static EventBus getEventBus() {
        return eventBus;
    }
}
