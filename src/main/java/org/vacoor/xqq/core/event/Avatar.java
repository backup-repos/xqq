package org.vacoor.xqq.core.event;

/**
 * User: vacoor
 */
public class Avatar {

    private final long uin;
    private final int type;
    private final byte[] bytes;

    public Avatar(long uin, int type, byte[] bytes) {
        this.uin = uin;
        this.type = type;
        this.bytes = bytes;
    }

    public long getUin() {
        return uin;
    }

    public int getType() {
        return type;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
