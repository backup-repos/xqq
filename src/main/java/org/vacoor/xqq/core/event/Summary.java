package org.vacoor.xqq.core.event;

/**
 * {"retcode":0,"result":[{"uin":4156523229,"lnick":"__硪 锝 幸 福 、与 爱 情 无 关 、（却） 与 迩 冇 关"}]}
 *
 * User: vacoor
 */
public class Summary {
    private long uin;
    private String lnick;

    public long getUin() {
        return uin;
    }

    public void setUin(long uin) {
        this.uin = uin;
    }

    public String getLnick() {
        return lnick;
    }

    public void setLnick(String lnick) {
        this.lnick = lnick;
    }
}
