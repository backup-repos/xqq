package org.vacoor.xqq.core.event;

import org.vacoor.xqq.core.bean.ClientType;
import org.vacoor.xqq.core.bean.Status;
import org.vacoor.xqq.core.poll.PollReply;

/**
 * 在线好友响应消息 / 好友状态改变消息
 * 在线好友响应:
 * {"retcode":0,"result":[{"uin":3229168883,"status":"callme","client_type":41},{"uin":41732157,"status":"online","client_type":21}]}
 * 好友状态改变响应:
 * {"retcode":0,"result":[{"poll_type":"buddies_status_change","value":{"uin":2160040239,"status":"offline","client_type":1}}]}
 * <p/>
 * User: vacoor
 */
public class BuddyStatus implements PollReply {
    private long uin;
    private Status status;
    private ClientType clientType;

    public long getUin() {
        return uin;
    }

    public void setUin(long uin) {
        this.uin = uin;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    @Override
    public String toString() {
        return getClass() + ":{" +
                "uin:" + uin +
                ", status:" + status +
                ", clientType:" + clientType
                + "}";
    }
}
