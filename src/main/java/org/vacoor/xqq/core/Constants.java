package org.vacoor.xqq.core;

/**
 */
public interface Constants {
    /**
     * APP ID SmartQQ = 501004106,WebQQ = 1003903
     */
    public static final String APP_ID = "501004106";
//    public static final String H_REFERER = "http://d1.web2.qq.com/proxy.html?v=20110331002&callback=1&id=1";
    public static final String H_REFERER = "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2";

    // 账户安全性校验(包括加密密钥的获取)
//    public static final String U_CHECK = "http://check.ptlogin2.qq.com/check";
    public static final String U_CHECK = "https://ssl.ptlogin2.qq.com/check?pt_tea=1&uin=272451590&appid=501004106&js_ver=10034&js_type=0&login_sig=&u1=http%3A%2F%2Fw.qq.com%2Fproxy.html&r=0.6418161285109818";


    // 验证码获取
    public static final String U_CAPTCHA = "http://captcha.qq.com/getimage";
    public static final String U_LOGIN = "http://ptlogin2.qq.com/login";
    /**
     * 登录回调URL WebQQ 为: http://web2.qq.com/loginproxy.html, 会自动转码不要转, 否则http client 2次转吗
     * 这里的登陆状态 webqq_type 会被最终的2次登陆状态覆盖
     */
    public static final String U_LOGIN_PROXY = "http://w.qq.com/proxy.html?login2qq=1&webqq_type=40";
    public static final String U_LOGIN2 = "http://d1.web2.qq.com/channel/login2";
    public static final String U_LOGOUT = "http://d1.web2.qq.com/channel/logout2";

    public static final String U_GET_BUDDIES = "http://s.web2.qq.com/api/get_user_friends2";
    public static final String U_GET_ONLINE_BUDDIES = "http://d1.web2.qq.com/channel/get_online_buddies2";
    public static final String U_GET_ADDITIONAL = "http://s.web2.qq.com/api/get_single_long_nick2";

    public static final String U_GET_DISCUS = "http://s.web2.qq.com/api/get_discus_list";
    public static final String U_GET_DISCU_INFO = "http://d1.web2.qq.com/channel/get_discu_info";

    public static final String U_GET_GROUPS = "http://s.web2.qq.com/api/get_group_name_list_mask2";
    public static final String U_GET_GROUP_INFO = "http://s.web2.qq.com/api/get_group_info_ext2";

    // 9 个负载服务器任选一个
    public static final String[] U_GET_AVATAR = {
            "http://face1.web.qq.com/cgi/svr/face/getface",
            "http://face2.web.qq.com/cgi/svr/face/getface",
            "http://face3.web.qq.com/cgi/svr/face/getface",
            "http://face4.web.qq.com/cgi/svr/face/getface",
            "http://face5.web.qq.com/cgi/svr/face/getface",
            "http://face6.web.qq.com/cgi/svr/face/getface",
            "http://face7.web.qq.com/cgi/svr/face/getface",
            "http://face8.web.qq.com/cgi/svr/face/getface",
            "http://face9.web.qq.com/cgi/svr/face/getface"
    };

    public static final String U_POLL = "http://d1.web2.qq.com/channel/poll2";

    public static final String U_SEND_BUDDY_MSG = "http://d1.web2.qq.com/channel/send_buddy_msg2";
    public static final String U_SEND_DISCU_MSG = "http://d1.web2.qq.com/channel/send_discu_msg2";
    public static final String U_SEND_GROUP_MSG = "http://d1.web2.qq.com/channel/send_qun_msg2";
}
