package org.vacoor.xqq.core.exception;

/**
 * Created by Administrator on 13-12-23.
 */
public class WebQQException extends RuntimeException {

    public WebQQException() {
        super();
    }

    public WebQQException(String message) {
        super(message);
    }

    public WebQQException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebQQException(Throwable cause) {
        super(cause);
    }

}
