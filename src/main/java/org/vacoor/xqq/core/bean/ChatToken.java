package org.vacoor.xqq.core.bean;

/**
 * User: vacoor
 */
public class ChatToken {
    private long uin;
    private long clientId;
    private String ptwebqq;
    private String vfwebqq;
    private String psessionId;

    public ChatToken(long uin, long clientId, String ptwebqq, String vfwebqq, String psessionId) {
        this.uin = uin;
        this.clientId = clientId;
        this.ptwebqq = ptwebqq;
        this.vfwebqq = vfwebqq;
        this.psessionId = psessionId;
    }

    public long getUin() {
        return uin;
    }

    public long getClientId() {
        return clientId;
    }

    public String getPtwebqq() {
        return ptwebqq;
    }

    public String getVfwebqq() {
        return vfwebqq;
    }

    public String getPsessionId() {
        return psessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChatToken chatToken = (ChatToken) o;

        if (clientId != chatToken.clientId) return false;
        if (uin != chatToken.uin) return false;
        if (psessionId != null ? !psessionId.equals(chatToken.psessionId) : chatToken.psessionId != null) return false;
        if (ptwebqq != null ? !ptwebqq.equals(chatToken.ptwebqq) : chatToken.ptwebqq != null) return false;
        if (vfwebqq != null ? !vfwebqq.equals(chatToken.vfwebqq) : chatToken.vfwebqq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (uin ^ (uin >>> 32));
        result = 31 * result + (int) (clientId ^ (clientId >>> 32));
        result = 31 * result + (ptwebqq != null ? ptwebqq.hashCode() : 0);
        result = 31 * result + (vfwebqq != null ? vfwebqq.hashCode() : 0);
        result = 31 * result + (psessionId != null ? psessionId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ChatToken{" +
                "uin=" + uin +
                ", clientId=" + clientId +
                ", ptwebqq='" + ptwebqq + '\'' +
                ", vfwebqq='" + vfwebqq + '\'' +
                ", psessionId='" + psessionId + '\'' +
                '}';
    }
}
