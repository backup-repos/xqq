package org.vacoor.xqq.core.bean;

import javax.swing.*;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 */
public abstract class BuddyPair extends Peer implements Sortable, Iterable<Buddy> {
    protected Map<Long, Buddy> members = new LinkedHashMap<Long, Buddy>();
    protected int sort;

    public BuddyPair(long id) {
        this(id, null, null, null, null);
    }

    public BuddyPair(long id, String name, String mark, String summary, Icon avatar) {
        super(id, name, mark, summary, avatar);
    }

    public BuddyPair addMember(Buddy buddy) {
        if (buddy != null) {
            members.put(buddy.getId(), buddy);
        }
        return this;
    }

    public Buddy getMember(long uin) {
        return members.get(uin);
    }

    public Buddy[] getMembers() {
        return members.values().toArray(new Buddy[members.size()]);
    }

    @Override
    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int size() {
        return members.size();
    }

    @Override
    public Iterator<Buddy> iterator() {
        return members.values().iterator();
    }
}
