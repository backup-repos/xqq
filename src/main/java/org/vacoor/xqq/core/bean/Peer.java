package org.vacoor.xqq.core.bean;

import org.vacoor.xqq.ui.util.ImageUtil;

import javax.swing.*;
import java.awt.*;

/**
 * 好友, 群, 讨论组 抽象类
 */
public abstract class Peer extends IdentifiableAndObservableData {
    public static final Dimension SMALL_AVATAR_SIZE = new Dimension(20, 20);    // 小头像尺寸
    public static final Dimension BIG_AVATAR_SIZE = new Dimension(40, 40);      // 大头像尺寸

    protected long no;              // QQ 号码, 群号码
    protected String name;          // 名称/昵称
    protected String mark;          // 好友备注, 群备注
    protected String summary;       // 签名, 群公告
    protected Icon avatar;          // 头像

    protected Icon smallAvatar;     // 小头像
    protected Icon bigAvatar;       // 大头像

    public Peer(long id) {
        this(id, null, null, null, null);
    }

    public Peer(long id, String name, String mark, String summary, Icon avatar) {
        super(id);
        setName(name);
        setMark(mark);
        setSummary(summary);
        setAvatar(avatar);
    }

    public long getNo() {
        return no;
    }

    public Peer setNo(long no) {
        long old = this.no;
        this.no = no;
        firePropertyChange("no", old, summary);
        return this;
    }

    public String getName() {
        return name;
    }

    public Peer setName(String name) {
        String old = this.name;
        this.name = name;
        firePropertyChange("name", old, summary);
        return this;
    }

    public String getMark() {
        return mark;
    }

    public Peer setMark(String mark) {
        String old = this.mark;
        this.mark = mark;
        firePropertyChange("mark", old, summary);
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public Peer setSummary(String summary) {
        String old = this.summary;
        this.summary = summary;
        firePropertyChange("summary", old, summary);
        return this;
    }

    public Icon getAvatar() {
        return avatar;
    }

    public Icon getSmallAvatar() {
        return smallAvatar;
    }

    public Icon getBigAvatar() {
        return bigAvatar;
    }

    public Peer setAvatar(Icon avatar) {
        Icon old = this.avatar;
        this.avatar = avatar;
        if (avatar != null) {
            prepareAvatar(((ImageIcon) avatar).getImage());
        }
        firePropertyChange("avatar", old, avatar);
        return this;
    }

    //
    protected void prepareAvatar(Image avatar) {
        smallAvatar = null;
        bigAvatar = null;
        if (avatar == null) {
            return;
        }

        smallAvatar = new ImageIcon(ImageUtil.resize(avatar, SMALL_AVATAR_SIZE));
        bigAvatar = new ImageIcon(ImageUtil.resize(avatar, BIG_AVATAR_SIZE));
    }
}
