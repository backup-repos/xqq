package org.vacoor.xqq.core.bean;

import java.util.Comparator;

/**
 */
public interface Sortable {

    int getSort();


    /**
     * 该类通过 {@link #getSort()} 进行排序, 因此需要格外注意, eg: TreeSet 将会通过 compare 结果来判断两个元素是否相同
     */
    public static class SortableComparator implements Comparator<Sortable> {

        @Override
        public int compare(Sortable o1, Sortable o2) {
            if (o1 == o2) {
                return 0;
            }
            if (o1 != null && o2 == null) {
                return 1;
            }
            if (o1 == null && o2 != null) {
                return -1;
            }
            return o1.getSort() > o2.getSort() ? 1 : (o1.getSort() < o2.getSort() ? -1 : 0);
        }
    }
}
