package org.vacoor.xqq.core.bean;

import org.vacoor.xqq.core.util.ImageResources;
import org.vacoor.xqq.ui.util.ImageUtil;

import javax.swing.*;
import java.awt.*;

/**
 * 好友
 *
 * @author: Vacoor
 */
public class Buddy extends Peer {
    public static final ImageIcon DEFAULT_AVATAR = ImageResources.getIcon("images/head/default.gif");
    public static final Image ICON_CLIENT_MOBILE = ImageResources.getImage("images/client_type/online_phone.png");
    public static final Image ICON_CLIENT_WEB = ImageResources.getImage("images/client_type/online_webqq.png");
    public static final Image ICON_CLIENT_PAD = ImageResources.getImage("images/client_type/online_pad.png");

    // web qq 状态标记
    public static final Image WEB_AWAY = ImageResources.getImage("images/status/flag/web_away.png");
    public static final Image WEB_BUSY = ImageResources.getImage("images/status/flag/web_busy.png");
    public static final Image WEB_MUTE = ImageResources.getImage("images/status/flag/web_mute.png");
    public static final Image WEB_QME = ImageResources.getImage("images/status/flag/web_qme.png");
    public static final Image WEB_ONLINE = ImageResources.getImage("images/status/flag/web_online.png");

    private Status status = Status.OFFLINE;
    private ClientType clientType = ClientType.UNKNOW;

    public Buddy(long uin) {
        this(uin, null, null, null, null);
    }

    public Buddy(long uin, String nick, String mark, String summary, Icon avatar) {
        super(uin);
        setNick(nick);
        setMark(mark);
        setSummary(summary);
        setAvatar(avatar != null ? avatar : DEFAULT_AVATAR);
    }

    public long getUin() {
        return getId();
    }

    public String getNick() {
        return getName();
    }

    public Buddy setNick(String nick) {
        String old = getName();
        setName(nick);
        firePropertyChange("nick", old, nick);
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public Buddy setStatus(Status status) {
        Status old = this.status;
        this.status = status;
        if (avatar != null) {
            prepareAvatar(((ImageIcon) avatar).getImage());
        }
        firePropertyChange("status", old, status);
        return this;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public Buddy setClientType(ClientType clientType) {
        ClientType old = this.clientType;
        this.clientType = clientType;
        if (avatar != null) {
            prepareAvatar(((ImageIcon) avatar).getImage());
        }
        firePropertyChange("clientType", old, clientType);
        return this;
    }

    @Override
    protected void prepareAvatar(Image avatar) {
        if (avatar == null) {
            return;
        }

        if (Status.isInvisible(status)) {
            // 默认头像就不处理了
//            avatar = this.avatar == DEFAULT_AVATAR ? avatar : ImageUtil.gray(avatar);
            avatar = this.avatar == DEFAULT_AVATAR ? avatar : ImageUtil.negative(avatar);
            super.prepareAvatar(avatar);
            return;
        }

        Image watermark = null;
        /** 水印位置 */
        switch (clientType) {
            case MOBILE:
                watermark = ICON_CLIENT_MOBILE;
                break;
            case WEBQQ:
                switch (status) {
                    case AWAY:
                        watermark = WEB_AWAY;
                        break;
                    case BUSY:
                        watermark = WEB_BUSY;
                        break;
                    case SILENT:
                        watermark = WEB_MUTE;
                        break;
                    case CALLME:
                        watermark = WEB_QME;
                        break;
                    default:    // ONLINE
                        watermark = WEB_ONLINE;
//                        watermark = ICON_CLIENT_WEB;
                }
                break;
            case PAD:
                watermark = ICON_CLIENT_PAD;
                break;
            default:    //PC
                watermark = ((ImageIcon) status.getIcon()).getImage();
                /*
                switch (status) {
                    case AWAY:
                        break;
                    case BUSY:
                        break;
                    case SILENT:
                        break;
                    case CALLME:
                    default: // Online
                }
                */
        }

        // 如果需要水印
        if (watermark != null) {
            int w = watermark.getWidth(null);
            int h = watermark.getHeight(null);
            int x = avatar.getWidth(null) - w - 2;
            int y = avatar.getHeight(null) - h - 2;
            avatar = ImageUtil.drawWatermark(avatar, watermark, x, y, w, h, null);
        }
        super.prepareAvatar(avatar);
    }

    @Override
    public String toString() {
        return (mark != null ? mark : name) + "(" + no + ")";
        /*
        return this.getClass().getCanonicalName() + ": {" +
                "uin:'" + getId() + '\'' +
                ",no:'" + no + '\'' +
                ",nick:'" + name + '\'' +
                ",mark:'" + mark + '\'' +
                ",summary:" + summary + '\'' +
                '}';
        */
    }
}
