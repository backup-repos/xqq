package org.vacoor.xqq.core.bean;

import org.vacoor.xqq.core.util.ImageResources;

import javax.swing.*;

/**
 */
public class Group extends BuddyPair {
    private static final ImageIcon DEFAULT_AVATAR = ImageResources.getIcon("images/head/group_avatar.png");

    /**
     * 获取群信息时需要使用, 发送消息使用 id, 接收消息使用 code
     */
    private long code;

    public Group(long gid, String name) {
        super(gid, name, null, null, DEFAULT_AVATAR);
    }

    public long getGid() {
        return getId();
    }

    public long getCode() {
        return code;
    }

    public Group setCode(long code) {
        this.code = code;
        return this;
    }
}
