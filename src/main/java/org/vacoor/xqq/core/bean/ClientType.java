package org.vacoor.xqq.core.bean;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;

/**
 * 客户端类型
 *
 * @author: vacoor
 */
@JsonDeserialize(using = ClientType.ClientTypeDeserializer.class)
public enum ClientType {
    PC,
    MOBILE,
    WEBQQ,
    PAD,
    UNKNOW;

    public static ClientType fromCode(int code) {
        ClientType clientType = UNKNOW;
        switch (code) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 10:
            case (int) 1E4:
                clientType = PC;
                break;
            case 21:
            case 22:
            case 23:
            case 24:
                clientType = MOBILE;
                break;
            case 41:
                clientType = WEBQQ;
                break;
            case 42:
                clientType = PAD;
                break;
            default:
//                clientType = UNKNOW;
        }
        return clientType;
    }

    /**
     * 根据客户端类型代码反序列化为 Enum
     */
    static final class ClientTypeDeserializer extends JsonDeserializer<ClientType> {
        @Override
        public ClientType deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            return fromCode(jp.getIntValue());
        }
    }
}
