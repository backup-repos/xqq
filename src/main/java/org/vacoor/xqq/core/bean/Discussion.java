package org.vacoor.xqq.core.bean;

import org.vacoor.xqq.core.util.ImageResources;

import javax.swing.*;

/**
 * 讨论组
 */
public class Discussion extends BuddyPair {
    private static final ImageIcon DEFAULT_AVATAR = ImageResources.getIcon("images/head/discu_avatar.png");

    public Discussion(long did, String name) {
        super(did);
        setName(name);
        setAvatar(DEFAULT_AVATAR);
    }

    public long getDid() {
        return getId();
    }
}
