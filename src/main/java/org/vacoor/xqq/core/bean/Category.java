package org.vacoor.xqq.core.bean;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 分类
 */
public class Category<T extends Peer> implements Sortable, Iterable<T> {

    private int index;
    private String name;    // 名称
    private int sort;
    private Map<Long, T> peers = new LinkedHashMap<Long, T>();

    public Category(int index, String name) {
        this.index = index;
        setName(name);
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public Category addPeer(T peer) {
        peers.put(peer.getId(), peer);
        return this;
    }

    public T getPeer(long uin) {
        return peers.get(uin);
    }

    public int size() {
        return peers.size();
    }

    @Override
    public Iterator<T> iterator() {
        return peers.values().iterator();
    }

    @Override
    public String toString() {
        return this.getClass().getCanonicalName() + ": {" +
                "index:" + index +
                ", name:'" + name + '\'' +
                ", sort:'" + sort + '\'' +
                '}';
    }
}
