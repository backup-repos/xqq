package org.vacoor.xqq.core.http;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: vacoor
 * Date: 10/15/13
 * Time: 10:20 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class RequestCallbackAdapter implements RequestCallback {
    @Override
    public void onSuccess(Response resp) throws IOException {}

    @Override
    public void onFailure(Response resp) {}

    @Override
    public void onException(Exception ex) {}

    @Override
    public void onCancel() {}
}
