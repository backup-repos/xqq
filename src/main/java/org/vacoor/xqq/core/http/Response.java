package org.vacoor.xqq.core.http;

import org.vacoor.xqq.core.exception.WebQQException;

import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Created with IntelliJ IDEA.
 * User: vacoor
 * Date: 10/9/13
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Response {

    int getStateCode();

    Content getContent();

    public static interface Content {

        byte[] asBytes() throws WebQQException;

        String asString() throws WebQQException;

        String asString(Charset charset) throws WebQQException;

        InputStream asStream() throws WebQQException;
    }

}
