package org.vacoor.xqq.core.http;

import java.util.LinkedList;
import java.util.List;

public class Request {

    private String url;
    private final HttpMethod method;
    private final List<Parameter> params = new LinkedList<Parameter>();
    private final List<Header> headers = new LinkedList<Header>();

    public Request(String url, HttpMethod method) {
        this.url = url;
        this.method = method;
    }

    public HttpMethod getMethod() {
        return this.method;
    }

    public String getURL() {
        return this.url;
    }

    public Request setURL(String url) {
        this.url = url;
        return this;
    }

    public Request addParameter(Parameter param) {
        params.add(param);
        return this;
    }

    public Request setParameter(Parameter param) {
        for (Parameter p : params) {
            if (p.getName().equalsIgnoreCase(param.getName())) {
                p.setValue(param.getValue());
                return this;
            }
        }
        params.add(param);
        return this;
    }

    public Request addParameter(String name, Object value) {
        return addParameter(new Parameter(name, value));
    }

    public Request setParameter(String name, String value) {
        return setParameter(new Parameter(name, value));
    }

    public Object getParameter(String name) {
        for (Parameter p : params) {
            if (p.getName().equalsIgnoreCase(name)) {
                return p.getValue();
            }
        }
        return null;
    }

    public String[] getParameters(String name) {
        StringBuffer vs = new StringBuffer();
        for (Parameter p : params) {
            if (p.getName().equalsIgnoreCase(name)) {
                if (vs.length() > 0) {
                    vs.append('&');
                }
                vs.append(p.getValue());
            }
        }
        return vs.length() == 0 ? null : vs.toString().split("&");
    }

    public Parameter[] getAllParameters() {
        return params.toArray(new Parameter[params.size()]);
    }

    public Header getHeader(String name) {
        for (Header h : headers) {
            if (h.getName().equalsIgnoreCase(name)) {
                return h;
            }
        }
        return null;
    }

    public Request addHeader(Header header) {
        headers.add(header);
        return this;
    }

    public Request setHeader(Header header) {
        for (Header h : headers) {
            if (h.getName().equalsIgnoreCase(header.getName())) {
                h.setValue(header.getValue());
                return this;
            }
        }
        headers.add(header);
        return this;
    }

    public Request addHeader(String name, String value) {
        return addHeader(new Header(name, value));
    }

    public Request setHeader(String name, String value) {
        return setHeader(new Header(name, value));
    }

    public Header[] getHeaders(String name) {
        return new Header[0];
    }

    public Header[] getAllHeaders() {
        return headers.toArray(new Header[headers.size()]);
    }

    // ----------------------

    public enum HttpMethod {
        GET, POST
    }

    public static class Header {
        // 常用的 HTTP 头
        public static final String HEADER_HOST = "Host";
        public static final String HEADER_REFERER = "Referer";
        public static final String HEADER_CONTENT_TYPE = "Content-Type";
        public static final String HEADER_COOKIE = "Cookie";
        public static final String HEADER_USER_AGENT = "User-Agent";

        public static final String HEADER_USER_AGENT_FF = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0";
        public static final String HEADER_USER_AGENT_IE6 = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";
        public static final String HEADER_USER_AGENT_IE8 = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)";

        private String name;
        private String value;

        public Header(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return name + ':' + value;
        }
    }

    public static class Parameter {
        private String name;
        private Object value;

        public Parameter(String name, Object value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public Object getValue() {
            return value;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }
}
