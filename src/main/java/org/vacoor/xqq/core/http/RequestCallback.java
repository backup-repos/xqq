package org.vacoor.xqq.core.http;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: vacoor
 * Date: 13-10-14
 * Time: 下午10:40
 * To change this template use File | Settings | File Templates.
 */
public interface RequestCallback {

    //状态码 200 时调用
    void onSuccess(Response resp) throws IOException;

    // 状态码非 200 时调用
    void onFailure(Response resp);

    // 执行异常
    void onException(Exception ex);

    void onCancel();

}
