package org.vacoor.xqq.core.msg;

import java.util.Iterator;

/**
 * 消息
 */
public interface Message extends Iterable<MessageElement> {

    MessageStyle getMessageStyle();

    Message setMessageStyle(MessageStyle style);

    Message addElement(MessageElement... element);

    @Override
    Iterator<MessageElement> iterator();

}
