package org.vacoor.xqq.core.msg;

/**
 * 可发送消息
 */
public interface SendableMessage {
    /**
     * 发送的目标类型
     */
    enum Type {
        BUDDY, DISCU, GROUP
    }

    /**
     * 获取消息目标类型, 只有当调用发送后才能获取
     */
    Type getType();

    /**
     * 获取发送目标
     */
    long getTo();

    SendableMessage addElement(MessageElement... e);

    SendableMessage setMessageStyle(MessageStyle style);

    Message getMessage();

    SendableMessage sendToBuddy(long uin);

    SendableMessage sendToDiscu(long did);

    SendableMessage sendToGroup(long groupUin);

}
