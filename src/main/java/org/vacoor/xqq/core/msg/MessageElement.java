package org.vacoor.xqq.core.msg;

/**
 * 好友 / 群 / 讨论组 消息内容元素(片段)
 * {"retcode":0,"result":[{"poll_type":"message","value":{"msg_id":4116,"from_uin":658263830,"to_uin":2214963100,"msg_id2":432876,"msg_type":9,"reply_ip":180064304,"time":1382590355,"content":[["font",{"size":10,"color":"0080c0","style":[0,0,0],"name":"\u5B8B\u4F53"}],"text,",["face",56],",",["offpic",{"success":1,"file_path":"/1a8fbdff-e638-4a29-abf5-d3be7f3d4b5b"}]," "]}}]}
 * <p/>
 * User: Vacoor
 */
public abstract class MessageElement {

    public static FaceElement createFaceElement(int id) {
        return new FaceElement(id);
    }

    public static TextElement createTextElement(String text) {
        return new TextElement(text);
    }

    public static OffPicElement createOffPicElement(boolean success, String filePath) {
        return new OffPicElement(success, filePath);
    }

    /**
     * 文本元素
     */
    public static class TextElement extends MessageElement {
        private String text;

        TextElement(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    /**
     * 表情元素
     */
    public static class FaceElement extends MessageElement {
        private int id;

        FaceElement(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {
            return "[表情" + id + ']';
        }
    }

    /**
     * 离线图片
     */
    public static class OffPicElement extends MessageElement {
        /**
         * http://d.web2.qq.com/channel/get_offpic2?
         * file_path=/518a6eff-7cc1-4b9d-9b93-bb18ccf5550e
         * &f_uin=1014452913 //from_uin
         * &clientid=3441922
         * &psessionid=8368046764001e636f6e6e7365727665725f77656271714031302e3132382e36362e31313500005e7600000611026e04007cb6ec836d0000000a40445144494b435766416d0000002856d18e47690e512382cf42bee3ca206a00fd7c5d61ccbbfbe38f507bac26683715c7efc388e2e6f9
         *
         */
        private boolean success;
        private String filePath;

        OffPicElement(boolean success, String filePath) {
            this.success = success;
            this.filePath = filePath;
        }

        public boolean isSuccess() {
            return success;
        }

        public String getFilePath() {
            return filePath;
        }

        @Override
        public String toString() {
            return "[离线图片: " + filePath + "]";
        }
    }
}
