package org.vacoor.xqq.core.msg;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.vacoor.nothing.ui.util.Colors;

import java.awt.*;
import java.io.IOException;
import java.util.Arrays;

/**
 * 消息内容样式 (字体/大小/粗体/斜体/下划线/颜色)
 * {"retcode":0,"result":[{"poll_type":"message","value":{"msg_id":4116,"from_uin":658263830,"to_uin":2214963100,"msg_id2":432876,"msg_type":9,"reply_ip":180064304,"time":1382590355,"content":[["font",{"size":10,"color":"0080c0","richStyle":[0,0,0],"name":"\u5B8B\u4F53"}],"text,",["face",56],",",["offpic",{"success":1,"file_path":"/1a8fbdff-e638-4a29-abf5-d3be7f3d4b5b"}]," "]}}]}
 * <p/>
 * <p/>
 * User: Vacoor
 */
@JsonPropertyOrder({"name", "size", "style", "color"})
@JsonIgnoreProperties({"bold", "italic", "underline"})
public class MessageStyle {
    public static final MessageStyle DEFAULT_STYLE = new MessageStyle(10);

    public static final int MIN_FONT_SIZE = 8;  // 允许的最小字体
    public static final int MAX_FONT_SIZE = 22; // 允许的最大字体

    @JsonProperty("name")
    private String fontFamily = "宋体";

    @JsonProperty("size")
    private int fontSize = MIN_FONT_SIZE;

    @JsonProperty("color")
    @JsonSerialize(using = HexColorSerializer.class)
    @JsonDeserialize(using = HexColorDeserializer.class)
    private Color color = Color.BLACK;

    @JsonProperty("style")
    private int[] biu = {0, 0, 0};

    public MessageStyle() {
    }

    public MessageStyle(int fontSize) {
        setFontSize(fontSize);
    }

    public MessageStyle(String fontFamily, int fontSize, boolean isBold, boolean isItalic, boolean isUnderline, Color color) {
        setFontFamily(fontFamily);
        setFontSize(fontSize);
        setBold(isBold);
        setItalic(isItalic);
        setUnderline(isUnderline);
        setColor(color);
    }

    // ---
    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        fontSize = Math.min(MAX_FONT_SIZE, fontSize);
        fontSize = Math.max(MIN_FONT_SIZE, fontSize);
        this.fontSize = fontSize;
    }

    public boolean isBold() {
        return this.biu[0] != 0;
    }

    public void setBold(boolean bold) {
        this.biu[0] = bold ? 1 : 0;
    }

    public boolean isItalic() {
        return this.biu[1] != 0;
    }

    public void setItalic(boolean italic) {
        this.biu[1] = italic ? 1 : 0;
    }

    public boolean isUnderline() {
        return this.biu[2] != 0;
    }

    public void setUnderline(boolean underline) {
        this.biu[2] = underline ? 1 : 0;
    }

    public void setBiu(int[] biu) {
        for (int i = 0; i < biu.length; i++) {
            this.biu[i] = biu[i] != 0 ? 1 : 0;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "[\"font\"" +
                ", size:" + fontSize +
                ", color:" + Colors.toHex(color) +
                ", style:" + Arrays.toString(biu) +
                ", name:'" + fontFamily + "']";

    }

    // --------------------

    /**
     * 十六进制颜色序列化与反序列化
     */
    private static final class HexColorDeserializer extends JsonDeserializer<Color> {

        @Override
        public Color deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return Colors.parseHtmlHexColor(jp.getValueAsString("000000"));
        }
    }

    private static final class HexColorSerializer extends JsonSerializer<Color> {
        @Override
        public void serialize(Color value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
            jgen.writeString(Colors.toHex(value));
        }
    }
}
