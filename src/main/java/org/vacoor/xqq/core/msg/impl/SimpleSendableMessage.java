package org.vacoor.xqq.core.msg.impl;

import org.vacoor.xqq.core.msg.Message;
import org.vacoor.xqq.core.msg.MessageElement;
import org.vacoor.xqq.core.msg.MessageStyle;
import org.vacoor.xqq.core.msg.SendableMessage;

/**
 */
public abstract class SimpleSendableMessage implements SendableMessage {
    protected long to;
    protected Type type;
    protected Message message = new SimpleMessage();

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public long getTo() {
        return to;
    }

    @Override
    public SimpleSendableMessage addElement(MessageElement... element) {
        message.addElement(element);
        return this;
    }

    @Override
    public SimpleSendableMessage setMessageStyle(MessageStyle messageStyle) {
        message.setMessageStyle(messageStyle);
        return this;
    }

    @Override
    public Message getMessage() {
        return message;
    }

    public SimpleSendableMessage sendToBuddy(long uin) {
        send(uin, Type.BUDDY);
        return this;
    }

    public SimpleSendableMessage sendToDiscu(long did) {
        send(did, Type.DISCU);
        return this;
    }

    public SimpleSendableMessage sendToGroup(long groupUin) {
        send(groupUin, Type.GROUP);
        return this;
    }

    protected void send(long to, Type type) {
        this.to = to;
        this.type = type;
        doSend();
    }

    protected abstract void doSend();
}
