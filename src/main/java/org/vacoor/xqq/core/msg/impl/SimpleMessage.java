package org.vacoor.xqq.core.msg.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vacoor.nothing.common.json.Jacksons;
import org.vacoor.xqq.core.msg.Message;
import org.vacoor.xqq.core.msg.MessageElement;
import org.vacoor.xqq.core.msg.MessageStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 联系人 / 群 / 讨论组 消息体
 * <p/>
 * 在 QQ 中内容样式也作为一个消息体的元素出现
 * 在发送时 样式应该为最后一个片段, 接收时样式应该是第一个片段
 * <p/>
 * <p/>
 * User: Vacoor
 */

@JsonSerialize(using = SimpleMessage.MsgBodySerializer.class)
@JsonDeserialize(using = SimpleMessage.SimpleMsgDeserializer.class)
public class SimpleMessage implements Message {
    private MessageStyle messageStyle = MessageStyle.DEFAULT_STYLE;
    private List<MessageElement> elements = new ArrayList<MessageElement>();

    @Override
    public MessageStyle getMessageStyle() {
        return messageStyle;
    }

    @Override
    public SimpleMessage setMessageStyle(MessageStyle messageStyle) {
        this.messageStyle = messageStyle;
        return this;
    }

    @Override
    public SimpleMessage addElement(MessageElement... element) {
        if (element == null) {
            return this;
        }
        for (MessageElement e : element) {
            this.elements.add(e);
        }
        return this;
    }

    @Override
    public Iterator<MessageElement> iterator() {
        return elements.iterator();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("{").append(messageStyle);
        for (MessageElement e : elements) {
            str.append(',').append(e);
        }
        return str.append("}").toString();
    }

    // ------------------------------------------------------

    /**
     * 消息体反序列化器
     */
    static final class SimpleMsgDeserializer extends JsonDeserializer<SimpleMessage> {
        private final Logger logger = LoggerFactory.getLogger(SimpleMsgDeserializer.class);

        @Override
        public SimpleMessage deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            SimpleMessage simpleMessage = new SimpleMessage();
            JsonNode tree = jp.readValueAsTree();
            if (tree.isArray()) {
                for (JsonNode node : tree) {
                    // 文本消息
                    if (node.isTextual()) {
                        simpleMessage.addElement(MessageElement.createTextElement(node.asText()));
                    } else if (node.isArray() && node.size() > 1) {
                        String type = node.get(0).asText();

                        // 数组内容为风格
                        if ("font".equalsIgnoreCase(type)) {
                            simpleMessage.setMessageStyle(Jacksons.deserialize(node.get(1).toString(), MessageStyle.class));

                            // 数组内容为表情
                        } else if ("face".equalsIgnoreCase(type)) {
                            simpleMessage.addElement(MessageElement.createFaceElement(node.get(1).asInt(1)));

                            // 数组内容为离线图片对象
                        } else if ("offpic".equalsIgnoreCase(type)) {
                            JsonNode offpic = node.get(1);

                            simpleMessage.addElement(MessageElement.createOffPicElement(offpic.get("success").asBoolean(), offpic.get("file_path").asText()));
                        } else {
                            logger.warn("未知消息: {}", node);
                        }
                    } else {
                        logger.warn("未知消息: {}", node);
                    }
                }
            }
            return simpleMessage;
        }
    }

    /**
     * 消息体序列化器, 将消息体中 样式/ 元素 转换为 QQ 所使用的数组
     */
    static final class MsgBodySerializer extends JsonSerializer<SimpleMessage> {
        @Override
        public void serialize(SimpleMessage value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeStartArray();
            if (value != null) {
                for (MessageElement fragment : value) {
                    if (fragment instanceof MessageElement.TextElement) {
                        jgen.writeString(((MessageElement.TextElement) fragment).getText());
                    } else if (fragment instanceof MessageElement.FaceElement) {
                        // 表情
                        jgen.writeStartArray();
                        jgen.writeString("face");
                        jgen.writeNumber(((MessageElement.FaceElement) fragment).getId());
                        jgen.writeEndArray();
                    } else {
                        // TODO 对于离线图片等如何处理??
                    }
                }
                // 字体 在发送消息中需要放在最后(否则字体后的内容会被忽略)
                jgen.writeStartArray();
                jgen.writeString("font");
                jgen.writeObject(value.getMessageStyle());
                jgen.writeEndArray();
            }
            jgen.writeEndArray();
        }
    }
}
