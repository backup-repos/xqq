package org.vacoor.xqq.core.mod.authc.impl;

import org.vacoor.xqq.core.bean.Status;
import org.vacoor.xqq.core.mod.authc.AuthenticationToken;

/**
 * QQ 登陆可以采用 QQ 号码, 也可以采用邮箱,
 * 但是在加密过程中使用的是QQ号码, 这里为了方便登陆时也使用 QQ号码
 */
public class UinPasswordCaptchaStatusToken implements AuthenticationToken {
    private long uin;
    private char[] password;
    private String captcha;
    private Status status;

    public UinPasswordCaptchaStatusToken(long uin, char[] password, String captcha, Status status) {
        this.uin = uin;
        this.password = password;
        this.captcha = captcha;
        this.status = status;
    }

    @Override
    public Long getPrincipal() {
        return getUin();
    }

    @Override
    public char[] getCredentials() {
        return getPassword();
    }

    public long getUin() {
        return uin;
    }

    public void setUin(long uin) {
        this.uin = uin;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
