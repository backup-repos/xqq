package org.vacoor.xqq.core.mod.authc.exception;

/**
 * Created by Administrator on 13-12-23.
 */
public class IncorrectCapchaException extends AuthenticationException {
    public IncorrectCapchaException() {
        super();
    }

    public IncorrectCapchaException(String message) {
        super(message);
    }

    public IncorrectCapchaException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectCapchaException(Throwable cause) {
        super(cause);
    }
}
