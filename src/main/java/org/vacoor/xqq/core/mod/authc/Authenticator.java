package org.vacoor.xqq.core.mod.authc;

import org.vacoor.xqq.core.mod.authc.exception.AuthenticationException;

/**
 */
public interface Authenticator {

    AccessToken login(AuthenticationToken token) throws AuthenticationException;

    void logout();

}
