package org.vacoor.xqq.core.mod.authc;

/**
 */
public interface AuthenticationToken {

    Object getPrincipal();

    Object getCredentials();

}