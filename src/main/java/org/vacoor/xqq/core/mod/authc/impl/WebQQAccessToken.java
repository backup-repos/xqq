package org.vacoor.xqq.core.mod.authc.impl;

import org.vacoor.xqq.core.bean.Status;
import org.vacoor.xqq.core.mod.authc.AbstractAccessToken;

public class WebQQAccessToken extends AbstractAccessToken {
    private static final String UIN_KEY = WebQQAccessToken.class.getName() + ".uin";
    private static final String CLIENT_ID_KEY = WebQQAccessToken.class.getName() + ".client_id";
    private static final String PTWEBQQ_KEY = WebQQAccessToken.class.getName() + ".ptwebqq";
    private static final String VFWEBQQ_KEY = WebQQAccessToken.class.getName() + ".vfwebqq";
    private static final String PSESSION_ID_KEY = WebQQAccessToken.class.getName() + ".psession_id";
    private static final String STATUS_KEY = WebQQAccessToken.class.getName() + ".status";

    public Long getUin() {
        return getTypedAttribute(UIN_KEY, Long.class);
    }

    public void setUin(Long uin) {
        this.setAttribute(UIN_KEY, uin);
    }

    public Long getClientId() {
        return getTypedAttribute(CLIENT_ID_KEY, Long.class);
    }

    public void setClientId(Long clientId) {
        setAttribute(CLIENT_ID_KEY, clientId);
    }

    public String getPtwebqq() {
        return getTypedAttribute(PTWEBQQ_KEY, String.class);
    }

    public void setPtwebqq(String ptwebqq) {
        setAttribute(PTWEBQQ_KEY, ptwebqq);
    }

    public String getVfwebqq() {
        return getTypedAttribute(VFWEBQQ_KEY, String.class);
    }

    public void setVfwebqq(String vfwebqq) {
        setAttribute(VFWEBQQ_KEY, vfwebqq);
    }

    public String getPsessionId() {
        return getTypedAttribute(PSESSION_ID_KEY, String.class);
    }

    public void setPsessionId(String psessionId) {
        setAttribute(PSESSION_ID_KEY, psessionId);
    }

    public Status getStatus() {
        return getTypedAttribute(STATUS_KEY, Status.class);
    }

    public void setStatus(Status status) {
        setAttribute(STATUS_KEY, status);
    }

    @SuppressWarnings({"unchecked"})
    protected <E> E getTypedAttribute(Object key, Class<E> type) {
        E found = null;
        Object o = this.getAttribute(key);
        if (o != null) {
            if (!type.isAssignableFrom(o.getClass())) {
                String msg = "Invalid object found in Session under key [" + key + "].  Expected type " +
                        "was [" + type.getName() + "], but the object under that key is of type " +
                        "[" + o.getClass().getName() + "].";
                throw new IllegalArgumentException(msg);
            }
            found = (E) o;
        }
        return found;
    }
}
