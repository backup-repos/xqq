package org.vacoor.xqq.core.mod.receviver;

/**
 * 消息接收器
 * 异步接收消息并派发消息
 * <p/>
 * User: vacoor
 */
public interface MessageReceiver {

    void start();

    /**
     * 停止接收消息
     */
    void shutdown();

}
