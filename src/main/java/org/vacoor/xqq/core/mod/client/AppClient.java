package org.vacoor.xqq.core.mod.client;

import org.vacoor.xqq.core.mod.authc.AccessToken;
import org.vacoor.xqq.core.mod.authc.AuthenticationToken;
import org.vacoor.xqq.core.mod.authc.exception.AuthenticationException;

/**
 */
public interface AppClient {

    Object getPricipal();

    Object getCredentials();

    AccessToken getAccessToken();

    AccessToken getAccessToken(boolean create);

    boolean isLogined();

    void login(AuthenticationToken token) throws AuthenticationException;

    void logout();

}
