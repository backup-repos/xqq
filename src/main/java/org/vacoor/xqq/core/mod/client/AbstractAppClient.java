package org.vacoor.xqq.core.mod.client;

import org.vacoor.xqq.core.mod.authc.AccessToken;
import org.vacoor.xqq.core.mod.authc.Authenticator;
import org.vacoor.xqq.core.mod.authc.impl.WebQQAccessToken;

/**
 */
public abstract class AbstractAppClient implements AppClient {
    protected Object pricipal;
    protected Object credentials;
    protected AccessToken accessToken;
    protected Authenticator authenticator;

    @Override
    public Object getPricipal() {
        return this.pricipal;
    }

    @Override
    public Object getCredentials() {
        return this.credentials;
    }

    public WebQQAccessToken getWebQQAccessToken() {
        return (WebQQAccessToken) getAccessToken();
    }

    public AccessToken getAccessToken() {
        return getAccessToken(true);
    }

    @Override
    public AccessToken getAccessToken(boolean create) {
        if (!create || accessToken != null) {
            return accessToken;
        }
        synchronized (AbstractAppClient.class) {
            if (accessToken == null) {
                accessToken = createAccessToken();
            }
        }
        return accessToken;
    }

    protected abstract AccessToken createAccessToken();

    @Override
    public boolean isLogined() {
        return accessToken.isAuthenticated();
    }

    public Authenticator getAuthenticator() {
        return authenticator;
    }

    public void setAuthenticator(Authenticator authenticator) {
        this.authenticator = authenticator;
    }
}
