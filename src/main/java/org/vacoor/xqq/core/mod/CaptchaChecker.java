package org.vacoor.xqq.core.mod;

public interface CaptchaChecker {

    /**
     * 校验是否需要验证码
     *
     * @return
     */
    String check();

    /**
     * 获取验证码
     *
     * @return
     */
    byte[] getCaptcha();
}
