package org.vacoor.xqq.core.mod.sender.impl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vacoor.nothing.common.json.Jacksons;
import org.vacoor.xqq.core.Constants;
import org.vacoor.xqq.core.http.HttpRequestor;
import org.vacoor.xqq.core.http.Request;
import org.vacoor.xqq.core.http.RequestCallbackAdapter;
import org.vacoor.xqq.core.http.Response;
import org.vacoor.xqq.core.mod.authc.AccessToken;
import org.vacoor.xqq.core.mod.authc.impl.WebQQAccessToken;
import org.vacoor.xqq.core.mod.sender.MessageSender;
import org.vacoor.xqq.core.msg.SendableMessage;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import static org.vacoor.xqq.core.Constants.*;

/**
 * 消息发送器简单实现
 *
 * @author: vacoor
 */
public class SimpleMessageSender implements MessageSender {
    private static final Logger logger = LoggerFactory.getLogger(SimpleMessageSender.class);

    private AtomicLong msgId;

    public SimpleMessageSender() {
        // 按照腾讯做法来生成初始的 MSG ID (使用当前时间13位毫秒的倒数4,5,6位)
        this.msgId = new AtomicLong(new Date().getTime() / Math.round(1E3) % Math.round(1E4) * Math.round(1E4));
    }

    @Override
    public long currentMessageId() {
        return msgId.get();
    }

    @Override
    public long nextMessageId() {
        return msgId.incrementAndGet();
    }

    @Override
    public void send(SendableMessage message, AccessToken token) {
        SendableMessage.Type type = message.getType();
        final String url = type == SendableMessage.Type.DISCU ? U_SEND_DISCU_MSG : (type == SendableMessage.Type.GROUP ? U_SEND_GROUP_MSG : U_SEND_BUDDY_MSG);
        final ObjectNode params = createMessageParams(message, (WebQQAccessToken) token);
        HttpRequestor.getInstance().send(
                new Request(url, Request.HttpMethod.POST)
                        .addHeader("Referer", Constants.H_REFERER)
                        .addHeader("ContentBody-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                        .addParameter("r", params),
                new RequestCallbackAdapter() {
                    @Override
                    public void onSuccess(Response resp) {
                        logger.debug("消息发送完成, 响应: {}", resp.getContent().asString());
                    }
                }
        );
    }

    /**
     */
    protected ObjectNode createMessageParams(SendableMessage message, WebQQAccessToken token) {
        ObjectNode params = Jacksons.createObjectNode();
        SendableMessage.Type type = message.getType();
        params.put(type == SendableMessage.Type.DISCU ? "did" : (type == SendableMessage.Type.GROUP ? "group_uin" : "to"), message.getTo());
        params.put("content", Jacksons.serialize(message.getMessage()));
        params.put("msgid", nextMessageId());
        params.put("clientid", token.getClientId());
        params.put("psessionid", token.getPsessionId());
        params.put("face", 525);
        return params;
    }
}
