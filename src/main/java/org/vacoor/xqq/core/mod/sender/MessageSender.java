package org.vacoor.xqq.core.mod.sender;

import org.vacoor.xqq.core.mod.authc.AccessToken;
import org.vacoor.xqq.core.msg.SendableMessage;

/**
 * 消息发送器
 */
public interface MessageSender {

    /**
     * 当前消息 ID
     */
    long currentMessageId();

    /**
     * 获取下一个消息 ID
     */
    long nextMessageId();

    /**
     * 使用给定 {@link org.vacoor.xqq.core.mod.authc.AccessToken} 发送一个消息到指定类型的对象
     */
    void send(SendableMessage msg, AccessToken token);

}
