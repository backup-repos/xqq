package org.vacoor.xqq.core.mod.authc.exception;

import org.vacoor.xqq.core.exception.WebQQException;

/**
 * Created by Administrator on 13-12-23.
 */
public class AuthenticationException extends WebQQException {
    public AuthenticationException() {
        super();
    }

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationException(Throwable cause) {
        super(cause);
    }
}
