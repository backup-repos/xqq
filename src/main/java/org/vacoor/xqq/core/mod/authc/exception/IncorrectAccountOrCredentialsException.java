package org.vacoor.xqq.core.mod.authc.exception;

public class IncorrectAccountOrCredentialsException extends AuthenticationException {

    public IncorrectAccountOrCredentialsException() {
        super();
    }

    public IncorrectAccountOrCredentialsException(String message) {
        super(message);
    }

    public IncorrectAccountOrCredentialsException(Throwable cause) {
        super(cause);
    }

    public IncorrectAccountOrCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }
}
