package org.vacoor.xqq.core.mod.authc;

import java.util.Collection;
import java.util.Date;

/**
 */
public interface AccessToken {

    boolean isAuthenticated();

    void setAuthenticated(boolean valid);

    Collection<Object> getAttributeNames();

    Object getAttribute(Object key);

    void setAttribute(Object key, Object value);

    Object removeAttribute(Object key);

    Date getCreatedDate();

}
