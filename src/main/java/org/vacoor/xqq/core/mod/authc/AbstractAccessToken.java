package org.vacoor.xqq.core.mod.authc;

import java.util.*;

/**
 */
public abstract class AbstractAccessToken implements AccessToken {
    private final Date createdDate;
    private Map<Object, Object> attributes;
    private boolean authenticated;


    public AbstractAccessToken() {
        this.attributes = new HashMap<Object, Object>();
        this.createdDate = new Date();
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    @Override
    public Collection<Object> getAttributeNames() {
        return Collections.unmodifiableCollection(attributes.keySet());
    }

    @Override
    public Object getAttribute(Object key) {
        return attributes.get(key);
    }

    @Override
    public void setAttribute(Object key, Object value) {
        attributes.put(key, value);
    }

    @Override
    public Object removeAttribute(Object key) {
        return attributes.remove(key);
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }
}
