package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.xqq.core.poll.impl.InputNotifyReply;

/**
 */
public class InputNotifyReplyResolver extends JsonReplyResolver<InputNotifyReply> {
    private static final String MESSAGE_TYPE = "input_notify";

    @Override
    public boolean supportsMessageType(String msgType) {
        return MESSAGE_TYPE.equalsIgnoreCase(msgType);
    }
}
