package org.vacoor.xqq.core.poll.impl;

/**
 * 讨论组消息
 * [{"poll_type":"discu_message","value":{"msg_id":25706,"from_uin":10000,"to_uin":2214963100,"msg_id2":863418,"msg_type":42,"reply_ip":176757014,"did":4081108480,"send_uin":3612030289,"seq":5,"time":1382522245,"info_seq":0,"simpleMessage":[["font",{"size":10,"color":"0080c0","richStyle":[0,0,0],"name":"宋体"}],"啊 "]}}]
 * <p/>
 * User: vacoor
 */

public class DiscuReply extends MulticastReply {
    private long did;

    public long getDid() {
        return did;
    }

    public void setDid(long did) {
        this.did = did;
    }

    @Override
    public String toStringFragment() {
        return ",did:" + did;
    }
}
