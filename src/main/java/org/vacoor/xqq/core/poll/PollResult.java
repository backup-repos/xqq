package org.vacoor.xqq.core.poll;

import com.fasterxml.jackson.databind.JsonNode;
import org.vacoor.nothing.common.json.Jacksons;
import org.vacoor.xqq.core.poll.poll.impl.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 */
public class PollResult {
    public static final int SC_MESSAGE = 0;        // 正常, 有消息
    public static final int SC_NOT_RELOGIN = 100;  // 没有重新登陆
    public static final int SC_NORMAL = 102;       // 正常
    public static final int SC_SELF_EXIT = 109;         // 主动退出
    public static final int SC_SELF_EXIT2 = 110;        // 主动退出

    /**
     * 服务器要求更新之后请求中 ptwebqq, 的值, cookie 中 ptwebqq 不更新
     * 从channel/login2 成功后, 每隔20分钟, 会返回116要求更换请求时的ptwebqq
     */
    public static final int SC_UPDATE_PTWEBQQ = 116;   // 更换请求中 ptwebqq 值, 不修改cookie ptwebqq
    public static final int SC_RELINK_FAIL = 120;      // 重新连接失败 需要重新登陆
    public static final int SC_RELINK_FAIL2 = 121;     //

    protected static final ArrayList<PollReplyResolver<?>> resolvers = new ArrayList<PollReplyResolver<?>>();

    static {
        resolvers.add(new BuddyReplyResolver());  // 好友消息解析器
        resolvers.add(new DiscuReplyResolver());  // 讨论组解析器
        resolvers.add(new GroupReplyResolver());  // 群解析
        resolvers.add(new StatusChangeReplyResolver());
        resolvers.add(new KickReplyResolver());   // 踢出消息
        resolvers.add(new InputNotifyReplyResolver());    // 输入唤醒解析
        resolvers.add(new ShakeReplyResolver());  // 窗口抖动
    }

    private JsonNode json;

    public PollResult(JsonNode json) {
        this.json = json;
    }

    public int getStatusCode() {
        JsonNode retcode = json.get("retcode");
        return retcode == null ? -1 << 10 : retcode.asInt();
    }

    public List<PollReply> getMessages() {
        JsonNode result = json.get("result");
        if (result == null) {
            return null;
        }

        List<PollReply> replies = new ArrayList<PollReply>();

        for (JsonNode msg : result) {
            final String msgType = msg.get("poll_type").asText();
            JsonNode content = msg.get("value");
            PollReplyResolver<?> resolver = getSupportMessageResolver(msgType);
            if (resolver != null) {
                PollReply pollReply = resolver.resolveMessage(content.toString());
                replies.add(pollReply);
            } else {
                // TODO LOG
            }
        }
        return Collections.unmodifiableList(replies);
    }

    public String getPtwebqq() {
        JsonNode p = json.get("p");
        return p == null ? null : p.asText();
    }


    protected PollReplyResolver<?> getSupportMessageResolver(String msgType) {
        for (PollReplyResolver<?> resolver : resolvers) {
            if (resolver.supportsMessageType(msgType)) {
                return resolver;
            }
        }
        return null;
    }

    public static PollResult formJson(String json) {
        return new PollResult(Jacksons.readTree(json));
    }


    //    resolvePoll()
    public static void main(String[] args) {
        String json = "{\"retcode\":0,\"result\":[{\"poll_type\":\"message\",\"value\":{\"msg_id\":12237,\"from_uin\":1068435874,\"to_uin\":2214963100,\"msg_id2\":780775,\"msg_type\":9,\"reply_ip\":178848397,\"time\":1387989544,\"content\":[[\"font\",{\"size\":10,\"color\":\"0080c0\",\"style\":[0,0,0],\"name\":\"\\u5FAE\\u8F6F\\u96C5\\u9ED1\"}],\"\\uFF1F \"]}}]}";
        json = "{\"retcode\":0, \"result\":[{\"poll_type\":\"discu_message\",\"value\":{\"msg_id\":25706,\"from_uin\":10000,\"to_uin\":2214963100,\"msg_id2\":863418,\"msg_type\":42,\"reply_ip\":176757014,\"did\":4081108480,\"send_uin\":3612030289,\"seq\":5,\"time\":1382522245,\"info_seq\":0,\"simpleMessage\":[[\"font\",{\"size\":10,\"color\":\"0080c0\",\"richStyle\":[0,0,0],\"name\":\"宋体\"}],\"啊 \"]}}]}";
        json = "{\"retcode\":0, \"result\":[{\"poll_type\":\"group_message\",\"value\":{\"msg_id\":25728,\"from_uin\":2198780971,\"to_uin\":2214963100,\"msg_id2\":129207,\"msg_type\":43,\"reply_ip\":180028747,\"group_code\":3755044557,\"send_uin\":3612030289,\"seq\":364,\"time\":1382522899,\"info_seq\":92085446,\"simpleMessage\":[[\"font\",{\"size\":10,\"color\":\"0080c0\",\"richStyle\":[0,0,0],\"name\":\"宋体\"}],\"wo qu  \",[\"face\",56],\" \"]}}]}";
        json = "{\"retcode\":0,\"result\":[{\"poll_type\":\"kick_message\",\"value\":{\"msg_id\":39817,\"from_uin\":10000,\"to_uin\":272451590,\"msg_id2\":39818,\"msg_type\":48,\"reply_ip\":0,\"show_reason\":1,\"reason\":\"\\u60A8\\u7684\\u5E10\\u53F7\\u5728\\u53E6\\u4E00\\u5730\\u70B9\\u767B\\u5F55\\uFF0C\\u60A8\\u5DF2\\u88AB\\u8FEB\\u4E0B\\u7EBF\\u3002\\u5982\\u6709\\u7591\\u95EE\\uFF0C\\u8BF7\\u767B\\u5F55 safe.qq.com \\u4E86\\u89E3\\u66F4\\u591A\\u3002\"}}]}";
        json = "{\"retcode\":0,\"result\":[{\"poll_type\":\"input_notify\",\"value\":{\"msg_id\":28825,\"from_uin\":42414820,\"to_uin\":272451590,\"msg_id2\":2446038176,\"msg_type\":121,\"reply_ip\":4294967295}}]}";
        PollResult pollResult = PollResult.formJson(json);
        switch (pollResult.getStatusCode()) {
            case PollResult.SC_MESSAGE:
                List<PollReply> replies = pollResult.getMessages();
                System.out.println(replies);
                break;
            case PollResult.SC_UPDATE_PTWEBQQ:
                System.out.println(pollResult.getPtwebqq());
                break;
            case PollResult.SC_NORMAL:
                System.out.println("normal");
                break;
            case PollResult.SC_SELF_EXIT:
            case PollResult.SC_SELF_EXIT2:
                System.out.println("exit");
                break;
            case PollResult.SC_NOT_RELOGIN:
            case PollResult.SC_RELINK_FAIL:
            case PollResult.SC_RELINK_FAIL2:
                break;
            default:
                System.out.println("error");
        }
    }
}
