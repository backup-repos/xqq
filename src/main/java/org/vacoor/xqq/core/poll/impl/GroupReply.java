package org.vacoor.xqq.core.poll.impl;

/**
 * 群消息
 * }
 * <p/>
 * User: vacoor
 */
public class GroupReply extends MulticastReply {
    private long groupCode;

    public long getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(long groupCode) {
        this.groupCode = groupCode;
    }

    @Override
    public String toStringFragment() {
        return ",group_code:" + groupCode;
    }
}
