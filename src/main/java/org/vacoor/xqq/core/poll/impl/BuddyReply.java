package org.vacoor.xqq.core.poll.impl;

/**
 * 好友消息
 * <p/>
 * {"retcode":0,"result":[{"poll_type":"message","value":{"msg_id":4116,"from_uin":658263830,"to_uin":2214963100,"msg_id2":432876,"msg_type":9,"reply_ip":180064304,"time":1382590355,"simpleMessage":[["font",{"size":10,"color":"0080c0","richStyle":[0,0,0],"name":"\u5B8B\u4F53"}],"text,",["face",56],",",["offpic",{"success":1,"file_path":"/1a8fbdff-e638-4a29-abf5-d3be7f3d4b5b"}]," "]}}]}
 * User: vacoor
 */
public class BuddyReply extends AbstractPollReply {

    @Override
    protected String toStringFragment() {
        return "";
    }

}
