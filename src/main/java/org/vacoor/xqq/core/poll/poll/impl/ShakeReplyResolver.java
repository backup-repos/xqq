package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.xqq.core.poll.impl.ShakeReply;

/**
 */
public class ShakeReplyResolver extends JsonReplyResolver<ShakeReply> {
    private static final String MESSAGE_TYPE = "shake_message";

    @Override
    public boolean supportsMessageType(String msgType) {
        return MESSAGE_TYPE.equalsIgnoreCase(msgType);
    }
}
