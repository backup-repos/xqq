package org.vacoor.xqq.core.poll.impl;

/**
 */
public abstract class MulticastReply extends AbstractPollReply {
    protected long sendUin; // 组中消息发送者
    protected long seq;
    protected long infoSeq; // 组编号( 群号码/ 讨论组号码)

    /**
     * 组播消息 from uin = 10000
     * @return
     */
    @Override
    public long getFromUin() {
        return super.getFromUin();
    }

    public long getSendUin() {
        return sendUin;
    }

    public void setSendUin(long sendUin) {
        this.sendUin = sendUin;
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public long getInfoSeq() {
        return infoSeq;
    }

    public void setInfoSeq(long infoSeq) {
        this.infoSeq = infoSeq;
    }

    @Override
    protected String toStringFragment() {
        return ",send_uin:" + sendUin + "," +
                "seq:" + seq + "," +
                "infoSeq:" + infoSeq;
    }
}
