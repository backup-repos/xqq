package org.vacoor.xqq.core.poll.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.vacoor.xqq.core.msg.impl.SimpleMessage;
import org.vacoor.xqq.core.poll.PollReply;

/**
 * 基本消息
 *
 * @author: vacoor
 */
public abstract class AbstractPollReply implements PollReply {
    protected long msgId;   // 消息ID
    protected long fromUin; // 发送人, 对于多播消息(群/讨论组)为10000
    protected long toUin;   // 发给谁, 一般就是当前QQ
    protected long msgId2;  // ??
    protected long time;    // 时间, 单位s

    /**
     * 允许消息体为空, 在反序列化时, 可以忽略该属性
     * 目前只有 好友消息 / 群组消息 / 讨论组消息 / 临时会话消息会有消息体
     */
    @JsonProperty("content")
    protected SimpleMessage message = new SimpleMessage();

    /**
     * TODO 是否可以通过消息类型知道类型而不必通过 poll_type
     * BuddyReply 9  DiscuReply 42 GroupReply 43 InputNotifyReply 121 KickReply 48 SessMsg 140
     */
    protected int msgType;
    protected long replyIp; // 发送者IP(10进制)

    public long getMsgId() {
        return msgId;
    }

    public void setMsgId(long msgId) {
        this.msgId = msgId;
    }

    public long getFromUin() {
        return fromUin;
    }

    public void setFromUin(long fromUin) {
        this.fromUin = fromUin;
    }

    public long getToUin() {
        return toUin;
    }

    public void setToUin(long toUin) {
        this.toUin = toUin;
    }

    public long getMsgId2() {
        return msgId2;
    }

    public void setMsgId2(long msgId2) {
        this.msgId2 = msgId2;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public long getReplyIp() {
        return replyIp;
    }

    public void setReplyIp(long replyIp) {
        this.replyIp = replyIp;
    }

    /**
     * jackson 根据返回类型反序列化, 因此..
     */
    public SimpleMessage getMessage() {
        return message;
    }

    public void setMessage(SimpleMessage message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return getClass().getCanonicalName() +
                ":{" +
                "msg_id:" + msgId + "," +
                "from_uin:" + fromUin + "," +
                "to_uin:" + toUin + "," +
                "msg_id2:" + msgId2 + "," +
                "time:" + time + "," +
                "msg_type:" + msgType + "," +
                "reply_ip:" + replyIp +
                toStringFragment() + "," +
                "content:" + message +
                '}';
    }

    /**
     * 用于在父类toString() 中追加属性toString
     */
    protected abstract String toStringFragment();
}
