package org.vacoor.xqq.core.poll.impl;

/**
 * 窗口抖动消息
 * <p/>
 * {"retcode":0,"result":[{"poll_type":"shake_message","value":{"msg_id":23196,"from_uin":3754218053,"to_uin":2214963100,"msg_id2":316261,"msg_type":9,"reply_ip":180064292}}]}
 * <p/>
 * User: vacoor
 */
public class ShakeReply extends AbstractPollReply {

    @Override
    protected String toStringFragment() {
        return "";
    }

}
