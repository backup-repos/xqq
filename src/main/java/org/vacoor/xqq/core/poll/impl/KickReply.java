package org.vacoor.xqq.core.poll.impl;

/**
 * 踢出消息(其他地方登录)
 * <p/>
 * {"retcode":0,"result":[{"poll_type":"kick_message","value":{"msg_id":39817,"from_uin":10000,"to_uin":272451590,"msg_id2":39818,"msg_type":48,"reply_ip":0,"show_reason":1,"reason":"\u60A8\u7684\u5E10\u53F7\u5728\u53E6\u4E00\u5730\u70B9\u767B\u5F55\uFF0C\u60A8\u5DF2\u88AB\u8FEB\u4E0B\u7EBF\u3002\u5982\u6709\u7591\u95EE\uFF0C\u8BF7\u767B\u5F55 safe.qq.com \u4E86\u89E3\u66F4\u591A\u3002"}}]}
 * <p/>
 * User: vacoor
 */

public class KickReply extends AbstractPollReply {
    private int showReason;
    private String reason;

    public int getShowReason() {
        return showReason;
    }

    public void setShowReason(int show_reason) {
        this.showReason = show_reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toStringFragment() {
        return ",show_reason:" + showReason + "," +
                "reason:'" + reason + '\'';
    }
}
