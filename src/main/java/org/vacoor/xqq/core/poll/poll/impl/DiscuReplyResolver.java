package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.xqq.core.poll.impl.DiscuReply;

/**
 */
public class DiscuReplyResolver extends JsonReplyResolver<DiscuReply> {
    private static final String MESSAGE_TYPE = "discu_message";

    @Override
    public boolean supportsMessageType(String msgType) {
        return MESSAGE_TYPE.equalsIgnoreCase(msgType);
    }
}
