package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.xqq.core.poll.PollReply;

/**
 * TODO 完成 system 类型消息处理
 */
public class SystemReplyResolver extends JsonReplyResolver<PollReply> {
    private static final String MESSAGE_TYPE = "system_message";

    @Override
    public boolean supportsMessageType(String msgType) {
        return MESSAGE_TYPE.equalsIgnoreCase(msgType);
    }

    @Override
    public PollReply resolveMessage(String json) {
        return null;
    }
}
