package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.xqq.core.poll.impl.BuddyReply;

/**
 */
public class BuddyReplyResolver extends JsonReplyResolver<BuddyReply> {
    private static final String MESSAGE_TYPE = "message";

    @Override
    public boolean supportsMessageType(String msgType) {
        return MESSAGE_TYPE.equalsIgnoreCase(msgType);
    }
}
