package org.vacoor.xqq.core.poll.impl;

/**
 * 好友输入状态消息
 * <p/>
 * {"retcode":0,"result":[{"poll_type":"input_notify","value":{"msg_id":28825,"from_uin":42414820,"to_uin":272451590,"msg_id2":2446038176,"msg_type":121,"reply_ip":4294967295}}]}
 * <p/>
 * User: vacoor
 */
public class InputNotifyReply extends AbstractPollReply {

    @Override
    protected String toStringFragment() {
        return "";
    }

}
