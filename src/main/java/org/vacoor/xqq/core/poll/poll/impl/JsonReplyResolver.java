package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.nothing.common.json.Jacksons;
import org.vacoor.xqq.core.poll.PollReply;
import org.vacoor.xqq.core.poll.PollReplyResolver;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 */
public abstract class JsonReplyResolver<T extends PollReply> implements PollReplyResolver<T> {
    private Type msgType;

    protected JsonReplyResolver() {
        Type type = getClass().getGenericSuperclass();
        if (type instanceof Class<?>) {
            throw new IllegalArgumentException("Internal error: TypeReference constructed without actual type information");
        }
        this.msgType = ((ParameterizedType) type).getActualTypeArguments()[0];

        /*
        Type genericType = ((ParameterizedType) type).getActualTypeArguments()[0];

        // simple class?
        if (type instanceof Class<?>) {
            Class<?> cls = (Class<?>) type;
        } else if (type instanceof ParameterizedType) {// But if not, need to start resolving.

        } else if (type instanceof GenericArrayType) {
        } else if (type instanceof TypeVariable<?>) {
        } else if (type instanceof WildcardType) {
        } else {
            // sanity check
            throw new IllegalArgumentException("Unrecognized Type: " + ((type == null) ? "[null]" : type.toString()));
        }
        */
    }

    @Override
    public T resolveMessage(String json) {
        return Jacksons.deserialize(json, Jacksons.constructType(msgType));
    }
}
