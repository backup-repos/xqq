package org.vacoor.xqq.core.poll;

/**
 */
public interface PollReplyResolver<T extends PollReply> {

    boolean supportsMessageType(String msgType);

    T resolveMessage(String value);

}
