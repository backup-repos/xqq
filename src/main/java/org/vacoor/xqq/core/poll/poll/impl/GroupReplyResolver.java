package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.xqq.core.poll.impl.GroupReply;

/**
 */
public class GroupReplyResolver extends JsonReplyResolver<GroupReply> {
    private static final String MESSAGE_TYPE = "group_message";

    @Override
    public boolean supportsMessageType(String msgType) {
        return MESSAGE_TYPE.equalsIgnoreCase(msgType);
    }

}
