package org.vacoor.xqq.core.poll.poll.impl;

import org.vacoor.xqq.core.event.BuddyStatus;

/**
 * Created by Administrator on 13-12-26.
 * TODO 修改 BuddyStatus
 */
public class StatusChangeReplyResolver extends JsonReplyResolver<BuddyStatus> {

    @Override
    public boolean supportsMessageType(String msgType) {
        return "buddies_status_change".equalsIgnoreCase(msgType);
    }

}
