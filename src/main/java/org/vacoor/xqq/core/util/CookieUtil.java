package org.vacoor.xqq.core.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: vacoor
 * Date: 10/10/13
 * Time: 10:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class CookieUtil {
    protected static final String GMT_TIME_ZONE_ID = "GMT";
    protected static final String COOKIE_DATE_FORMAT_STRING = "EEE, dd-MMM-yyyy HH:mm:ss z";

    public static String toCookieDate(int maxAge) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, maxAge);
        return toCookieDate(cal.getTime());
    }

    public static String toCookieDate(Date date) {
        TimeZone tz = TimeZone.getTimeZone(GMT_TIME_ZONE_ID);
        DateFormat fmt = new SimpleDateFormat(COOKIE_DATE_FORMAT_STRING, Locale.US);
        fmt.setTimeZone(tz);
        return fmt.format(date);
    }

    public static void main(String[] args) {
        System.out.println(toCookieDate(100));
    }
}
