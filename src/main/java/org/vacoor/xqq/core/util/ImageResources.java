package org.vacoor.xqq.core.util;


import org.vacoor.nothing.common.misc.util.Resources;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 */
public class ImageResources {

    public static ImageIcon getIcon(String res) {
        URL url = Resources.getResource(res);
        return url == null ? null : new ImageIcon(url);
    }

    public static Image getImage(String res) {
        ImageIcon icon = getIcon(res);
        return icon == null ? null : icon.getImage();
    }

}
