package org.vacoor.xqq.core.util;

import java.io.File;
import java.io.FileFilter;
import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: vacoor
 * Date: 13-10-8
 * Time: 下午10:41
 * To change this template use File | Settings | File Templates.
 */
public class SqliteDBStore {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        File file = new File(System.getProperty("user.home") + File.separator + ".mozilla" + File.separator + "firefox");
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory() && pathname.getName().endsWith(".default");
            }
        });
        for(File f : files) {
            System.out.println(f);
        }
        if(files.length == 1) {
            file = files[0];
            File[] cookieDB = file.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.isFile() && "cookies.sqlite".equalsIgnoreCase(pathname.getName());
                }
            });
            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + cookieDB[0]);
            Statement st = conn.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM moz_cookies");
            ResultSetMetaData metaData = resultSet.getMetaData();
            int count = metaData.getColumnCount();
            for( int i = 1; i < count + 1; i++ ) {
                String name = metaData.getColumnName(i);
                String type = metaData.getColumnTypeName(i);
                System.out.println(name + ":" + type);
            }
            while(resultSet.next()) {
            }
//            conn.prepareStatement()
        }
    }
}
