Ubuntu 系统图盘说明
查看
	# gsettings get com.canonical.Unity.Panel systray-whitelist
	# ['JavaEmbeddedFrame', 'Wine', 'Update-notifier'] # 虽然 JavaEmbededFrame 已经在白名单当中, 但是还是不行

修改为所有
	gsettings set com.canonical.Unity.Panel systray-whitelist "['all']" # 修改为所有, 重启即可

重置
   gsettings reset com.canonical.Unity.panel systray-whitelist
